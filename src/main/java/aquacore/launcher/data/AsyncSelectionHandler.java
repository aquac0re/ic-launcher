package aquacore.launcher.data;

import java.util.HashSet;
import java.util.Set;

public class AsyncSelectionHandler<I, O> {
	private volatile O response = null;
	private final ISelector<I, O> selector;
	private final I requestData;
	private final Set<Consumer<O>> okConsumers = new HashSet<>();
	private final Set<Consumer<SelectionException>> errorConsumers = new HashSet<>();

	public static interface Consumer<T> {
		public void consume(T data);
	}

	AsyncSelectionHandler(ISelector<I, O> selector, I requestData) {
		this.selector = selector;
		this.requestData = requestData;
	}

	public AsyncSelectionHandler<I, O> then(Consumer<O> consumer) {
		okConsumers.add(consumer);
		return this;
	}

	public AsyncSelectionHandler<I, O> onError(Consumer<SelectionException> consumer) {
		errorConsumers.add(consumer);
		return this;
	}

	public void execute() {
		new Thread() {
			{
				setName("AsyncSelect_" + selector.getClass().getName());
			}

			public void run() {
				try {
					response = selector.select(requestData);
					for (Consumer<O> consumer : okConsumers) {
						consumer.consume(response);
					}
				} catch (SelectionException e) {
					for (Consumer<SelectionException> consumer : errorConsumers) {
						consumer.consume(e);
					}
				}
			};
		}.start();
	}

}

package aquacore.launcher.data;

public class ClientSelectionException extends SelectionException {
	private static final long serialVersionUID = 1L;

	public ClientSelectionException(String message) {
		super(message);
	}

	public ClientSelectionException(String message, Throwable cause) {
		super(message, cause);
	}

}

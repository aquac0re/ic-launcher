package aquacore.launcher.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;

import oshi.SystemInfo;
import oshi.hardware.ComputerSystem;
import oshi.hardware.Display;
import oshi.hardware.HWDiskStore;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.hardware.NetworkIF;

public class HWIDSelector implements ISelector<Void, String> {

	public static class HWID {
		private static String cachedFields = null;

		private static void getFields(MultiValuedMap<String, String> output) {
			try {
				SystemInfo si = new SystemInfo();
				HardwareAbstractionLayer hal = si.getHardware();
				ComputerSystem cs = hal.getComputerSystem();
				try {
					String bbsn = cs.getBaseboard().getSerialNumber();
					if (bbsn != null && !bbsn.isEmpty() && !bbsn.equalsIgnoreCase("unknown")) {
						output.put("motherboard_sn", bbsn);
					}
				} catch (Throwable e) {
					;
				}
				try {
					output
							.put("motherboard_name", cs.getBaseboard().getManufacturer() + " "
									+ cs.getBaseboard().getModel() + " " + cs.getBaseboard().getVersion());
				} catch (Throwable e) {
					;
				}
				try {
					output.put("cpu_name", hal.getProcessor().getIdentifier());
				} catch (Throwable e) {
					;
				}

				try {
					for (HWDiskStore disk : hal.getDiskStores()) {
						try {
							String hdsn = disk.getSerial();
							if (hdsn != null && !hdsn.isEmpty() && !hdsn.equalsIgnoreCase("unknown")) {
								output.put("hdd_sn", hdsn);
							}
						} catch (Throwable e) {
							;
						}
					}
				} catch (Throwable e) {
					;
				}
				try {
					for (NetworkIF iface : hal.getNetworkIFs()) {
						try {
							String mac = iface.getMacaddr();
							output.put("mac", mac);
						} catch (Throwable e) {
							;
						}
					}
				} catch (Throwable e) {
					;
				}
				try {
					for (Display disp : hal.getDisplays()) {

						try {
							byte[] edid = disp.getEdid();
							if (edid == null || edid.length < 16)
								continue;
							byte[] slice = Arrays.copyOfRange(edid, 0, 16);
							output.put("edid_header", Hex.encodeHexString(slice));
						} catch (Throwable e) {
							;
						}
					}
				} catch (Throwable e) {
					;
				}
			} catch (Throwable e) {
				;
			}
		}

		public static String constructFields() {
			if (cachedFields == null) {
				HashSetValuedHashMap<String, String> fields = new HashSetValuedHashMap<>();
				getFields(fields);
				List<String> strs = new ArrayList<>();
				for (Map.Entry<String, String> entry : fields.entries()) {
					strs.add(entry.getKey() + "@@VAL@@" + entry.getValue());
				}
				CharSequence[] css = new CharSequence[strs.size()];
				strs.toArray(css);
				cachedFields = String.join("@@SEP@@", css);
			}

			return cachedFields;
		}
	}

	@Override
	public String select(Void input) {
		return HWID.constructFields();
	}

}

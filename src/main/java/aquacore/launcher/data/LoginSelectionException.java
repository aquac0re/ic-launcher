package aquacore.launcher.data;

public class LoginSelectionException extends SelectionException {
	private static final long serialVersionUID = 1L;

	public LoginSelectionException(String message) {
		super(message);
	}

	public LoginSelectionException(String message, Throwable cause) {
		super(message, cause);
	}

}

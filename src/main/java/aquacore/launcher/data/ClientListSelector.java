package aquacore.launcher.data;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import aquacore.launcher.update.ClientSpec;
import aquacore.launcher.util.Config;

public class ClientListSelector implements ISelector<Void, Map<String, ClientSpec>> {

	@Override
	public Map<String, ClientSpec> select(Void input) throws SelectionException {
		HttpClient cl = HttpClientBuilder.create().build();
		HttpGet req = new HttpGet(Config.launchServer + "/clients");
		try {
			HttpResponse response = cl.execute(req);
			Gson gson = new Gson();
			Map<String, ClientSpec> m = gson.fromJson(new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8), new TypeToken<Map<String, ClientSpec>>() {}.getType());
			m.values().forEach(ClientSpec::doPing);
			return m;
		} catch (IOException e) {
			throw new ClientSelectionException("Error retrieving client list: ", e);
		}
	}
}

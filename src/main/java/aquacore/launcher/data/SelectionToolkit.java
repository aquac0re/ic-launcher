package aquacore.launcher.data;

import java.util.HashMap;
import java.util.Map;

public class SelectionToolkit {

	private static Map<Class<ISelector<Object, Object>>, ISelector<Object, Object>> selectorMap = new HashMap<>();

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <I, O, T extends ISelector<I, O>> O selectSync(Class<? extends T> selector, I input) throws SelectionException {
		if (selectorMap.containsKey(selector)) {
			return ((T) selectorMap.get(selector)).select(input);
		} else {
			try {
				T si = selector.newInstance();
				O ret = si.select(input);
				((Map)selectorMap).put(selector, si); // FUCKING JAVA GENERICS
				return ret;
			} catch (InstantiationException | IllegalAccessException e) {
				throw new InstantiationError(e.getMessage());
			}
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <I, O> AsyncSelectionHandler<I, O> selectAsync(Class<? extends ISelector<I, O>> selector, I input) {
		if (selectorMap.containsKey(selector)) {
			return new AsyncSelectionHandler<I, O>((ISelector<I, O>) selectorMap.get(selector), input);
		} else {
			try {
				ISelector<I, O> si = selector.newInstance();
				((Map)selectorMap).put(selector, si); // FUCKING JAVA GENERICS
				return new AsyncSelectionHandler<I, O>(si, input);
			} catch (InstantiationException | IllegalAccessException e) {
				throw new InstantiationError(e.getMessage());
			}
		}
	}

}

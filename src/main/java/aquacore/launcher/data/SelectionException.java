package aquacore.launcher.data;

public abstract class SelectionException extends Exception {
	private static final long serialVersionUID = 1L;

	public SelectionException() {
	}

	public SelectionException(String message) {
		super(message);
	}

	public SelectionException(Throwable cause) {
		super(cause);
	}

	public SelectionException(String message, Throwable cause) {
		super(message, cause);
	}

	public SelectionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}

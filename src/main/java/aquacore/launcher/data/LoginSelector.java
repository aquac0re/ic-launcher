package aquacore.launcher.data;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.json.JSONException;
import org.json.JSONObject;

import aquacore.launcher.data.LoginSelector.AuthParameter;
import aquacore.launcher.data.LoginSelector.AuthResult;
import aquacore.launcher.launch.CryptoUtils;
import lombok.Getter;
import lombok.SneakyThrows;

public class LoginSelector implements ISelector<AuthParameter, AuthResult> {
	public static class AuthResult {
		public String accessToken = null;
		public String uuid = null;
		public String username = null;
	}

	@Getter
	public static class AuthParameter {
		private String username;
		private String password;
		private String hwid;
		
		private AuthParameter(String username, String password, String hwid) {
			this.username = username;
			this.password = password;
			this.hwid = hwid;
		}

		@SneakyThrows(SelectionException.class)
		public static AuthParameter of(String username, String password) {
			return new AuthParameter(username, password, SelectionToolkit.selectSync(HWIDSelector.class, null));
		}

		public static AuthParameter of(String username, String password, String hwid) {
			return new AuthParameter(username, password, hwid);
		}
	}

	@Override
	public AuthResult select(AuthParameter input) throws SelectionException {
		InputStream is = null;
		JSONObject json = null;
		AuthResult res = new AuthResult();
		try {
			JSONObject obj = new JSONObject();

			obj.put("username", input.getUsername());
			obj.put("password", input.getPassword());
			obj.put("hwid", input.getHwid());

			String str = new String(CryptoUtils.cryptoRequest(obj.toString().getBytes(), "login"), Charset.forName("UTF-8"));
			
			json = new JSONObject(str);
			if (json.has("error") && !json.isNull("error")) {
				throw new LoginSelectionException(json.getString("error"));
			} else {
				res.accessToken = json.getString("accessToken");
				res.uuid = json.getString("uuid");
				res.username = input.getUsername();
			}
		} catch (JSONException | IOException e) {
			throw new LoginSelectionException(e.getMessage(), e);
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e1) {
				throw new LoginSelectionException(e1.getMessage(), e1);
			}
		}

		return res;
	}

}

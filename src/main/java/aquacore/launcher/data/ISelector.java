package aquacore.launcher.data;

public interface ISelector<I, O> {
	O select(I input) throws SelectionException;
}

package aquacore.launcher.applet;

import java.applet.AppletContext;
import java.applet.AppletStub;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

@SuppressWarnings("rawtypes")
public class AquaCoreAppletStub implements AppletStub {
	final Map arguments;

	public AquaCoreAppletStub(Map arg0) {
		this.arguments = arg0;
	}

	public void appletResize(int arg0, int arg1) {
	}

	public boolean isActive() {
		return true;
	}

	public URL getDocumentBase() {
		try {
			return new URL("http://www.minecraft.net/game/");
		} catch (MalformedURLException arg1) {
			arg1.printStackTrace();
			return null;
		}
	}

	public String getParameter(String arg0) {
		if (this.arguments.containsKey(arg0)) {
			return (String) this.arguments.get(arg0);
		} else {
			System.err.println("Client asked for parameter: " + arg0);
			return null;
		}
	}

	@Override
	public URL getCodeBase() {
		return getDocumentBase();
	}

	@Override
	public AppletContext getAppletContext() {
		return null;
	}
}

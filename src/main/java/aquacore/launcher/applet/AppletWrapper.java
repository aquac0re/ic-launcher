package aquacore.launcher.applet;

import java.applet.Applet;
import java.applet.AppletStub;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.HashMap;

import javax.swing.JPanel;

import aquacore.launcher.ui.LauncherWindow;
import net.minecraft.launchwrapper.Launch;

public class AppletWrapper extends JPanel {
	public AppletWrapper() {
		setLayout(new BorderLayout(0, 0));
	}
	
	private static final long serialVersionUID = 1L;
	
	public Applet applet;
	public AppletStub stub;
	public Thread shutdownHook;

	public void runApplet(Applet applet, String shutdownClass, String[] par0ArrayOfStr) {
		HashMap<String, String> var1 = new HashMap<String, String>();
		boolean var2 = false;
		boolean var3 = true;
		boolean var4 = false;
		String var5 = "Player" + System.currentTimeMillis() % 1000L;
		String var6 = var5;

		if (par0ArrayOfStr.length > 0) {
			var6 = par0ArrayOfStr[0];
		}

		String var7 = "-";

		if (par0ArrayOfStr.length > 1) {
			var7 = par0ArrayOfStr[1];
		}

		for (int var9 = 2; var9 < par0ArrayOfStr.length; ++var9) {
			String var10 = par0ArrayOfStr[var9];
			boolean var12 = false;

			if (!var10.equals("-demo") && !var10.equals("--demo")) {
				if (var10.equals("--applet")) {
					var3 = false;
				}
			} else {
				var2 = true;
			}

			if (var12) {
				++var9;
			}
		}

		if (var6.contains("@") && var7.length() <= 1) {
			var6 = var5;
		}

		var1.put("demo", "" + var2);
		var1.put("stand-alone", "" + var3);
		var1.put("username", var6);
		var1.put("fullscreen", "" + var4);
		var1.put("sessionid", var7);
		this.applet = applet;
		applet.setStub(stub = new AquaCoreAppletStub(var1));
		add(applet, BorderLayout.CENTER);
		applet.setBackground(Color.BLACK);
		LauncherWindow.instance.setCurrentPane(this);
		if (shutdownHook != null) {
			Runtime.getRuntime().removeShutdownHook(shutdownHook);
		}
		if (shutdownClass != null) {
			try {
				Runtime
						.getRuntime()
						.addShutdownHook(shutdownHook = (Thread) Class.forName(shutdownClass, true, Launch.classLoader).newInstance());
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		applet.init();
		applet.start();
	}
	
	public void replace(Applet applet) {
		if(this.applet != null) {
			this.applet.stop();
			this.applet.destroy();
			remove(this.applet);
		}
		applet.setStub(this.stub);
		applet.setBackground(Color.BLACK);
		this.applet = applet;
		LauncherWindow.instance.setCurrentPane(this);
		applet.setSize(getSize());
		add(applet, BorderLayout.CENTER);
		applet.init();
		applet.start();
	}

	@Override
	public void removeAll() {
		return;
	}
}

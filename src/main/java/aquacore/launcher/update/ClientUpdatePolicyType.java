package aquacore.launcher.update;

import com.google.gson.annotations.SerializedName;

import aquacore.launcher.DontObfuscate;

@DontObfuscate
public enum ClientUpdatePolicyType {
	@SerializedName("directory")
	DIRECTORY,
	@SerializedName("file")
	FILE;
}

package aquacore.launcher.update;

import aquacore.launcher.DontObfuscate;

@DontObfuscate
public enum ClientUpdatePolicyMode {
	CLIENT_ONLY {
		@Override
		public ClientUpdatePolicyAction resolveAction(ClientUpdatePolicy policy) {
			return policy.clientOnlyPolicy;
		}
	},
	SERVER_ONLY {
		@Override
		public ClientUpdatePolicyAction resolveAction(ClientUpdatePolicy policy) {
			return policy.serverOnlyPolicy;
		}
	},
	DIFFERENT {
		@Override
		public ClientUpdatePolicyAction resolveAction(ClientUpdatePolicy policy) {
			return policy.differentPolicy;
		}
	};

	public abstract ClientUpdatePolicyAction resolveAction(ClientUpdatePolicy policy);
}

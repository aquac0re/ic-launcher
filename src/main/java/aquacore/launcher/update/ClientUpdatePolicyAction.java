package aquacore.launcher.update;

import com.google.gson.annotations.SerializedName;

import aquacore.launcher.DontObfuscate;

@DontObfuscate
public enum ClientUpdatePolicyAction {
	@SerializedName("none")
	NONE {
		@Override
		public void addActionToQueue(HashState hashState, ClientChecker checker) {
			// Do nothing
		}
	},
	@SerializedName("download")
	DOWNLOAD {
		@Override
		public void addActionToQueue(HashState hashState, ClientChecker checker) {
			checker.getDownloadQueue().addDownload(checker.getDownload(hashState));
		}
	},
	@SerializedName("update")
	UPDATE {
		@Override
		public void addActionToQueue(HashState hashState, ClientChecker checker) {
			checker.getDownloadQueue().addDownload(checker.getDownload(hashState));
		}
	},
	@SerializedName("remove")
	REMOVE {
		@Override
		public void addActionToQueue(HashState hashState, ClientChecker checker) {
			checker.getRemoveQueue().add(checker.getFile(hashState));
		}
	};

	public abstract void addActionToQueue(HashState hashState, ClientChecker checker);
}

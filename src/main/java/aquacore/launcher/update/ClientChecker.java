package aquacore.launcher.update;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.codec.binary.Hex;

import aquacore.launcher.util.Config;
import aquacore.launcher.util.DownloadQueue;
import aquacore.launcher.util.DownloadQueue.DownloadInfo;
import lombok.Getter;

@Getter
public class ClientChecker {

	private static MessageDigest md;

	private String name;
	private ClientSpec spec;
	private DownloadQueue downloadQueue = new DownloadQueue();
	private List<File> removeQueue = new ArrayList<File>();
	private HashSet<HashState> clientOnlySet;
	private HashSet<HashState> serverOnlySet;
	private HashSet<HashState> differentSet;

	public ClientChecker(String name, ClientSpec spec) {
		super();
		this.name = name;
		this.spec = spec;
	}

	static {
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			md = null;
		}
	}

	public DownloadInfo getDownload(HashState hs) {
		try {
			URL url = new URL((Config.launchServer + "/client/" + name + "/" + hs.name));
			URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(),
					url.getQuery(), url.getRef());
			return new DownloadInfo(uri.toURL(), getFile(hs));
		} catch (MalformedURLException | URISyntaxException e) {
			throw new RuntimeException(e);
		}

	}

	public File getFile(HashState hs) {
		try {
			return new File(ClientSpec.getDirectory(name), hs.name);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}

	private Set<HashState> buildFileSet(File root, File from) throws IOException {
		Set<HashState> set = new HashSet<>();
		File[] l = from.listFiles();
		if (l != null) {
			for (File file : l) {
				if (!Files.isSymbolicLink(file.toPath())) {
					if (file.isDirectory()) {
						set.addAll(buildFileSet(root, file));
					} else if (file.isFile()) {
						String path = root.toPath()
								.toAbsolutePath()
								.relativize(file.toPath().toAbsolutePath())
								.toString()
								.replace(File.separatorChar, '/');
						System.out.println(path);
						md.reset();
						FileInputStream fis = new FileInputStream(file);
						int count = 0;
						byte[] b = new byte[4096];
						while ((count = fis.read(b)) >= 0) {
							md.update(b, 0, count);
						}
						fis.close();
						set.add(new HashState(path, Hex.encodeHexString(md.digest()).toLowerCase()));
					}
				}
			}
		}
		return set;
	}

	private Set<HashState> getServerFileSet() throws IOException {
		Set<HashState> set = new HashSet<>();

		URL hashUrl = new URL(Config.launchServer + "/client/" + name + "/indexes.txt");
		InputStream is = hashUrl.openStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);

		String line;

		while ((line = br.readLine()) != null) {
			HashState hs = new HashState();

			String[] spl = line.split("  ", 2);

			if (spl.length == 2) {
				hs.hash = spl[0];
				hs.name = spl[1];
				set.add(hs);
			}

		}

		return set;
	}

	public void doCheck() throws URISyntaxException, IOException {
		File root = ClientSpec.getDirectory(name);

		Set<HashState> clientSet = buildFileSet(root, root);
		clientOnlySet = new HashSet<>(clientSet);
		Set<HashState> serverSet = getServerFileSet();
		serverOnlySet = new HashSet<>(serverSet);
		differentSet = new HashSet<>(serverSet);

		clientOnlySet.removeIf(chs -> serverSet.stream().anyMatch(hs -> hs.name.equals(chs.name)));
		serverOnlySet.removeIf(chs -> clientSet.stream().anyMatch(hs -> hs.name.equals(chs.name)));
		differentSet.removeIf(chs -> serverOnlySet.stream().anyMatch(hs -> hs.name.equals(chs.name)));
		differentSet.removeIf(chs -> clientSet.stream().anyMatch(hs -> hs.equals(chs)));

		clientOnlySet.stream()
				.forEach(hs -> spec.resolveAction(hs.name, ClientUpdatePolicyMode.CLIENT_ONLY)
						.addActionToQueue(hs, this));
		serverOnlySet.stream()
				.forEach(hs -> spec.resolveAction(hs.name, ClientUpdatePolicyMode.SERVER_ONLY)
						.addActionToQueue(hs, this));
		differentSet.stream()
				.forEach(hs -> spec.resolveAction(hs.name, ClientUpdatePolicyMode.DIFFERENT)
						.addActionToQueue(hs, this));
	}

	public void doRemoval() {
		removeQueue.stream().forEach(File::delete);
	}
}

package aquacore.launcher.update;

import java.io.File;
import java.net.URISyntaxException;
import java.util.List;

import aquacore.launcher.DontObfuscate;
import aquacore.launcher.launch.ClientLaunchMode;
import aquacore.launcher.util.Utils;
import aquacore.launcher.util.Config;

@DontObfuscate
public class ClientSpec {
	public String name;
	public String versionSpec;
	public String pingIP;
	public int pingPort;
	public List<ClientUpdatePolicy> policies;
	public List<String> launchArguments;
	public ClientLaunchMode launchMode;
	public List<String> appletArguments;
	public String appletShutdown;
	public String classpathDir;
	public String nativesDir;
	public boolean isDefault = false;
	
	public transient boolean online = false;
	public transient String status = "?/?";
	
	public static File getDirectory(String clientName) throws URISyntaxException {
		return new File(Config.getClientDir(), clientName);
	}
	
	public void doPing() {
		status = Utils.pingServer(pingIP, pingPort);
		online = !status.equals("?/?");
	}
	
	public ClientUpdatePolicyAction resolveAction(String path, ClientUpdatePolicyMode mode) {
		ClientUpdatePolicyAction action = ClientUpdatePolicyAction.NONE;
		for(ClientUpdatePolicy policy : policies) {
			if(policy.type == ClientUpdatePolicyType.DIRECTORY) {
				if(policy.paths.stream().anyMatch(p -> p.isEmpty() || path.startsWith(p + "/"))) {
					action = mode.resolveAction(policy);
				}
			} else if(policy.type == ClientUpdatePolicyType.FILE) {
				if(policy.paths.stream().anyMatch(p -> path.equals(p))) {
					action = mode.resolveAction(policy);
				}
			}
		}
		return action;
	}
}
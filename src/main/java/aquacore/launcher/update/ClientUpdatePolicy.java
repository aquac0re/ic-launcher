package aquacore.launcher.update;

import java.util.List;

import aquacore.launcher.DontObfuscate;

@DontObfuscate
public class ClientUpdatePolicy {
	public ClientUpdatePolicyType type;
	public List<String> paths;
	public ClientUpdatePolicyAction clientOnlyPolicy;
	public ClientUpdatePolicyAction serverOnlyPolicy;
	public ClientUpdatePolicyAction differentPolicy;
}

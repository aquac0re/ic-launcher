package aquacore.launcher.update;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class HashState {
	public String name;
	public String hash;

	@Override
	public boolean equals(Object e) {
		try {
			HashState hs = (HashState) e;
			return name.equals(hs.name) && hash.equals(hs.hash);
		} catch (Exception ex) {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HashState [name=" + name + ", hash=" + hash + "]";
	}
}

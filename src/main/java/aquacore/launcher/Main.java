package aquacore.launcher;

import aquacore.launcher.launch.LauncherRelauncher;

public class Main {
	@Deprecated
	public static void crash(Exception e) {
		System.out.println("=== LAUNCHER CRASH REPORT ===");
		e.printStackTrace();
		System.exit(1);
	}

	public static void main(String[] args) {
		LauncherRelauncher.relaunch();
	}

}

package aquacore.launcher.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import aquacore.launcher.data.LoginSelector.AuthResult;
import aquacore.launcher.util.Config;
import net.miginfocom.swing.MigLayout;

public class SkinsPane extends JPanel {
	private static final long serialVersionUID = 1L;
	
	public static final SkinsPane instance = new SkinsPane();
	public static final Component renderInstance = new MiddleParent(instance);
	
	public AquaCoreFlatButton backButton;
	public SkinPreviewPane skinPreviewPane;
	private AuthResult ares;
	private AquaCoreLabel errorLabel;
	private AquaCoreFlatButton aquaCoreFlatButton_1;
	private AquaCoreFlatButton aquaCoreFlatButton_2;

	/**
	 * Create the panel.
	 */
	public SkinsPane() {
		setBackground(Color.WHITE);
		
		setPreferredSize(new Dimension(405, 435));
		setMaximumSize(new Dimension(405, 435));
		setMinimumSize(new Dimension(405, 435));
		
		setLayout(new MigLayout("", "50[grow]20[grow]50", "50[50]:push[]:push[]20[30]20[30]20[30]50"));
		
		AquaCoreLabel aquaCoreLabel = new AquaCoreLabel((String) null, true);
		aquaCoreLabel.setHorizontalAlignment(SwingConstants.CENTER);
		aquaCoreLabel.setText("Сменить скин");
		add(aquaCoreLabel, "cell 0 0 2 1,grow");
		
		skinPreviewPane = new SkinPreviewPane();
		add(skinPreviewPane, "cell 0 1 2 1,alignx center,aligny center");
		
		errorLabel = new AquaCoreLabel((String) null);
		errorLabel.setText("");
		errorLabel.setForeground(new Color(Colors.color_error_d));
		add(errorLabel, "cell 0 2 2 1,grow");
		
		AquaCoreFlatButton aquaCoreFlatButton = new AquaCoreFlatButton(1);
		aquaCoreFlatButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser jfc = new JFileChooser();
				jfc.setFileFilter(new FileNameExtensionFilter("*.png", "png"));
				jfc.showDialog(SkinsPane.this, "Загрузить");
				HttpClient cl = HttpClientBuilder.create().build();
				HttpPost post = new HttpPost(Config.launchServer + "/skinul");
				MultipartEntityBuilder meb = MultipartEntityBuilder.create();
				meb.addBinaryBody("skin", jfc.getSelectedFile());
				meb.addTextBody("token", ares.accessToken);
				meb.addTextBody("uuid", ares.uuid);
				post.setEntity(meb.build());
				try {
					HttpResponse resp = cl.execute(post);
					JSONObject obj = new JSONObject(EntityUtils.toString(resp.getEntity()));
					if(!obj.isNull("error")) {
						errorLabel.setText(obj.getString("error"));
						return;
					}
				} catch (IOException | JSONException e2) {
					e2.printStackTrace();
					errorLabel.setText("Неизвестная ошибка");
					return;
				}
				
				try {
					skinPreviewPane.setImageURL(jfc.getSelectedFile().toURI().toURL());
				} catch (MalformedURLException e1) {
					e1.printStackTrace();
					errorLabel.setText("Неизвестная ошибка");
					return;
				}
				
				errorLabel.setText("");
			}
		});
		
		aquaCoreFlatButton.setText("Загрузить новый");
		add(aquaCoreFlatButton, "cell 0 3 2 1,grow");
		
		aquaCoreFlatButton_1 = new AquaCoreFlatButton();
		aquaCoreFlatButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(JOptionPane.showConfirmDialog(SkinsPane.this, "Вы уверены, что хотите сбросить скин?", "Вы уверены?", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
					return;
				}
				HttpClient cl = HttpClientBuilder.create().build();
				HttpPost post = new HttpPost(Config.launchServer + "/skinul");
				MultipartEntityBuilder meb = MultipartEntityBuilder.create();
				meb.addTextBody("token", ares.accessToken);
				meb.addTextBody("uuid", ares.uuid);
				post.setEntity(meb.build());
				try {
					HttpResponse resp = cl.execute(post);
					JSONObject obj = new JSONObject(EntityUtils.toString(resp.getEntity()));
					if(!obj.isNull("error")) {
						errorLabel.setText(obj.getString("error"));
						return;
					}
				} catch (IOException | JSONException e2) {
					e2.printStackTrace();
					errorLabel.setText("Неизвестная ошибка");
					return;
				}
				
				skinPreviewPane.setImageURL(null);
				
				errorLabel.setText("");
			}
		});
		aquaCoreFlatButton_1.setText("Сбросить");
		add(aquaCoreFlatButton_1, "flowx,cell 0 4,grow");
		
		aquaCoreFlatButton_2 = new AquaCoreFlatButton();
		aquaCoreFlatButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser jfc = new JFileChooser();
				jfc.setSelectedFile(new File("skin.png"));
				jfc.setFileFilter(new FileNameExtensionFilter("*.png", "png"));
				jfc.showSaveDialog(SkinsPane.this);
				try {
					FileUtils.copyInputStreamToFile(skinPreviewPane.getImageURL().openStream(), jfc.getSelectedFile());
				} catch (IOException e1) {
					e1.printStackTrace();
					errorLabel.setText("Неизвестная ошибка");
					return;
				}
			}
		});
		aquaCoreFlatButton_2.setText("Скачать");
		add(aquaCoreFlatButton_2, "cell 1 4,grow");
		
		backButton = new AquaCoreFlatButton();
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LauncherWindow.instance.back();
			}
		});
		backButton.setText("Назад");
		add(backButton, "cell 0 5 2 1,grow");

	}

	public void setURLByUUID(String uuid) {
		try {
			skinPreviewPane.setImageURL(new URL(Config.launchServer + "/client/Skins/" + uuid + ".png"));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		errorLabel.setText("");
	}
	

	public void initAuth(AuthResult res) {
		this.ares = res;
	}
}

package aquacore.launcher.ui;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.JProgressBar;

import net.miginfocom.swing.MigLayout;

public class LoadingPane extends JPanel {
	private static final long serialVersionUID = 1L;
	
	public static final LoadingPane instance = new LoadingPane();
	public static final Component renderInstance = new MiddleParent(instance);

	private LabelX labelX;

	private JProgressBar progressBar;

	/**
	 * Create the panel.
	 */
	public LoadingPane() {
		setBackground(Color.WHITE);
		setLayout(new MigLayout("", "25[350]25", "15[30]0[30]25"));
		
		labelX = new LabelX("Loading");
		labelX.setForeground(new Color(Colors.color_accent));
		add(labelX, "cell 0 0,grow");
		
		progressBar = new JProgressBar();
		progressBar.setBorderPainted(false);
		progressBar.setForeground(new Color(Colors.color_daccent));
		progressBar.setBackground(new Color(Colors.color_dbg));
		add(progressBar, "cell 0 1,grow");

	}
	
	public void setTitle(String title) {
		labelX.setText(title);
	}

	/**
	 * @param n
	 * @see javax.swing.JProgressBar#setValue(int)
	 */
	public void setValue(int n) {
		progressBar.setValue(n);
	}

	/**
	 * @param n
	 * @see javax.swing.JProgressBar#setMinimum(int)
	 */
	public void setMinimum(int n) {
		progressBar.setMinimum(n);
	}

	/**
	 * @param n
	 * @see javax.swing.JProgressBar#setMaximum(int)
	 */
	public void setMaximum(int n) {
		progressBar.setMaximum(n);
	}

	/**
	 * @param newValue
	 * @see javax.swing.JProgressBar#setIndeterminate(boolean)
	 */
	public void setIndeterminate(boolean newValue) {
		progressBar.setIndeterminate(newValue);
	}

}

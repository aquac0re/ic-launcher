package aquacore.launcher.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import aquacore.launcher.launch.LauncherRelauncher;
import aquacore.launcher.util.LauncherProperties;
import aquacore.launcher.util.OsUtils;
import net.miginfocom.swing.MigLayout;

public class SettingsPane extends JPanel {
	private static final long serialVersionUID = 1L;
	
	public static final SettingsPane instance = new SettingsPane();
	public static final Component renderInstance = new MiddleParent(instance);

	public AquaCoreFlatButton backButton;

	public SettingsPane() {
		setBackground(new Color(255, 255, 255, 204));
		setPreferredSize(new Dimension(405, 435));
		setMaximumSize(new Dimension(405, 435));
		setMinimumSize(new Dimension(405, 435));
		setLayout(new MigLayout("", "50[100][grow]50", "50[][20][]:push[35]50"));

		AquaCoreLabel memoryTitleLabel = new AquaCoreLabel("Выделение памяти:");
		memoryTitleLabel.setForeground(new Color(Colors.color_accent));
		add(memoryTitleLabel, "cell 0 0 2 1,grow");

		AquaCoreLabel memoryValueLabel = new AquaCoreLabel("загрузка");
		memoryValueLabel.setForeground(new Color(Colors.color_accent));
		final NumberSetter setter = new NumberSetter(512, (OsUtils.isWindows() && !OsUtils.is64()) ? 1024 : 8192,
				memoryValueLabel);
		setter.setCurrentNumber(Integer.parseInt(LauncherProperties.instance.getProperty("xmx")));
		add(setter, "cell 0 1 2 1,grow");
		add(memoryValueLabel, "cell 0 2 2 1,grow");

		backButton = new AquaCoreFlatButton();
		backButton.setText("Назад");
		add(backButton, "cell 0 3,grow");
		backButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setter.setCurrentNumber(Integer.parseInt(LauncherProperties.instance.getProperty("xmx")));
				LauncherWindow.instance.back();
			}
		});

		AquaCoreFlatButton saveButton = new AquaCoreFlatButton(1);
		saveButton.setText("Сохранить");
		add(saveButton, "cell 1 3,grow");
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LauncherProperties.instance.setProperty("xmx", String.valueOf(setter.getNumber()));
				LauncherProperties.instance.store();
				LauncherRelauncher.relaunch();
			}
		});
	}

}

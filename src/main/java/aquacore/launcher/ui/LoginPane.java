package aquacore.launcher.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.nio.charset.StandardCharsets;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.apache.commons.codec.binary.Base64;

import aquacore.launcher.data.AsyncSelectionHandler.Consumer;
import aquacore.launcher.data.ClientListSelector;
import aquacore.launcher.data.LoginSelector;
import aquacore.launcher.data.LoginSelector.AuthParameter;
import aquacore.launcher.data.LoginSelector.AuthResult;
import aquacore.launcher.data.SelectionException;
import aquacore.launcher.data.SelectionToolkit;
import aquacore.launcher.launch.ThreadClientLauncher;
import aquacore.launcher.update.ClientSpec;
import aquacore.launcher.util.LauncherProperties;
import net.miginfocom.swing.MigLayout;

public class LoginPane extends JPanel {
	private static final long serialVersionUID = 1L;

	public static final LoginPane instance = new LoginPane();
	public static final Component renderInstance = new LoginMiddleParent(instance, RegisterPanel.instance);

	public AquaCoreFlatButton settingsButton;
	public AquaCoreTextField loginField;
	public AquaCorePassField passField;
	public AquaCoreFlatButton skinsButton;
	private AquaCoreLabel l_error;

	private ThreadClientLauncher launchThread;

	@SuppressWarnings("deprecation")
	private void doAuth(Consumer<AuthResult> okConsumer) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				LoadingPane.instance.setIndeterminate(true);
				LoadingPane.instance.setTitle("Авторизация...");
				LauncherWindow.instance.setCurrentPane(LoadingPane.renderInstance);
			}
		});
		SelectionToolkit.selectAsync(LoginSelector.class, AuthParameter.of(loginField.getText(), passField.getText()))
				.then(okConsumer)
				.onError(new Consumer<SelectionException>() {
					public void consume(final SelectionException data) {
						data.printStackTrace();
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								l_error.setText(data.getMessage());
								l_error.setVisible(true);
								LauncherWindow.instance.setCurrentPane(LoginPane.renderInstance);
							}
						});
					};
				})
				.execute();
	}

	/**
	 * Create the panel.
	 */
	public LoginPane() {
		setBackground(new Color(255, 255, 255));
		setPreferredSize(new Dimension(405, 435));
		setMaximumSize(new Dimension(405, 435));
		setMinimumSize(new Dimension(405, 435));
		setLayout(new MigLayout("", ":push[250:250:250]:push",
				":push[30]0[30]10[30]0[30]10[30]0[30]0[35]0[30]20[30]20[30]:push"));

		AquaCoreLabel loginLabel = new AquaCoreLabel("Логин:");
		add(loginLabel, "cell 0 0,grow");
		loginLabel.setForeground(new Color(Colors.color_daccent));

		loginField = new AquaCoreTextField(loginLabel);
		loginField.setText(LauncherProperties.instance.getProperty("username"));
		add(loginField, "cell 0 1,grow");

		AquaCoreLabel passLabel = new AquaCoreLabel("Пароль:");
		add(passLabel, "cell 0 2,grow");
		passLabel.setForeground(new Color(Colors.color_daccent));

		passField = new AquaCorePassField(passLabel);
		passField.setText(new String(Base64.decodeBase64(LauncherProperties.instance.getProperty("password"))));
		add(passField, "cell 0 3,grow");

		AquaCoreLabel aquaCoreLabel_1 = new AquaCoreLabel("Сервер:");
		add(aquaCoreLabel_1, "cell 0 4,grow");
		aquaCoreLabel_1.setForeground(new Color(Colors.color_daccent));

		final JComboBox<Map.Entry<String, ClientSpec>> l_scCbox = new JComboBox<Map.Entry<String, ClientSpec>>();
		l_scCbox.setRenderer(new AquaCoreLCR());
		add(l_scCbox, "cell 0 5,grow");

		l_error = new AquaCoreLabel("");
		l_error.setForeground(new Color(Colors.color_error));
		add(l_error, "cell 0 6,grow");

		final AquaCoreFlatButton loginButton = new AquaCoreFlatButton("Войти", 1);
		loginButton.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				if (launchThread == null || !launchThread.isAlive()) {
					LauncherProperties.instance.setProperty("username", loginField.getText());
					LauncherProperties.instance.setProperty("password", Base64
							.encodeBase64String(passField.getText().getBytes(StandardCharsets.UTF_8)));
					LauncherProperties.instance.store();
					try {
						AuthResult res = SelectionToolkit.selectSync(LoginSelector.class, AuthParameter
								.of(loginField.getText(), passField.getText()));
						l_error.setVisible(false);
						@SuppressWarnings("unchecked")
						Map.Entry<String, ClientSpec> selectedClient = (Entry<String, ClientSpec>) l_scCbox
								.getSelectedItem();
						launchThread = new ThreadClientLauncher(selectedClient.getKey(), selectedClient.getValue(), res);
						launchThread.start();
					} catch (SelectionException e2) {
						l_error.setText(e2.getMessage());
						l_error.setVisible(true);
					}

				}

			}
		});
		add(loginButton, "cell 0 7,grow");

		settingsButton = new AquaCoreFlatButton();
		settingsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LauncherWindow.instance.pushBackLayerAndSwitchTo(SettingsPane.renderInstance);
			}
		});
		settingsButton.setText("Настройки");
		add(settingsButton, "cell 0 8,grow");

		skinsButton = new AquaCoreFlatButton();
		skinsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doAuth(new Consumer<AuthResult>() {
					@Override
					public void consume(final AuthResult data) {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								l_error.setVisible(false);
								SkinsPane.instance.initAuth(data);
								LauncherWindow.instance
										.pushBackLayerAndSwitchTo(SkinsPane.renderInstance, LoginPane.renderInstance);
							}
						});
					}
				});
			}
		});
		skinsButton.setText("Сменить скин");
		add(skinsButton, "cell 0 9,grow");

		SelectionToolkit.selectAsync(ClientListSelector.class, null).then(new Consumer<Map<String, ClientSpec>>() {

			@Override
			public void consume(final Map<String, ClientSpec> data) {

				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						l_scCbox.removeAllItems();
						if (data == null) {
							/*
							 * ClientBox nocb = new ClientBox(); nocb.id = ""; nocb.name = "Ошибка";
							 * nocb.pingHost = "188.18.54.44"; nocb.pingPort = 25800;
							 * l_scCbox.addItem(nocb);
							 */
						} else {
							Map.Entry<String, ClientSpec> current = null;
							for (Map.Entry<String, ClientSpec> cb : data.entrySet()) {
								l_scCbox.addItem(cb);
								if (cb.getKey().equals(LauncherProperties.instance.getProperty("client"))) {
									current = cb;
								}
							}

							if (current == null) {
								current = data.entrySet()
										.stream()
										.filter(e -> e.getValue().isDefault)
										.findFirst()
										.orElseGet(() -> new AbstractMap.SimpleEntry<>(null, null));
							}

							if (current != null) {
								LauncherProperties.instance.setProperty("client", current.getKey());
								LauncherProperties.instance.store();
								l_scCbox.setSelectedItem(current);
								loginButton.setText("Войти (" + current.getValue().status + ")");
								// getRootPane().repaint(); // Dirty hack
							}

							l_scCbox.addItemListener(new ItemListener() {

								@SuppressWarnings("unchecked")
								@Override
								public void itemStateChanged(ItemEvent e) {
									if (e.getStateChange() == ItemEvent.SELECTED) {
										if (e.getItem() != null) {
											LauncherProperties.instance
													.setProperty("client", ((Map.Entry<String, ClientSpec>) e.getItem())
															.getKey());
											LauncherProperties.instance.store();
											// Main.main(new String[] {});
											// now using chdir in runtime
										}
									}
								}

							});
						}
					}
				});
			}
		}).onError(new Consumer<SelectionException>() {

			@Override
			public void consume(SelectionException data) {
				data.printStackTrace();
			}
		}).execute();
	}

	public void setErrorText(String text) {
		l_error.setText(text);
	}
}

package aquacore.launcher.ui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

import aquacore.launcher.launch.LaunchCrashReporter;

public class SkinPreviewPane extends JComponent {
	private static final long serialVersionUID = 1L;

	private BufferedImage bim;
	private URL imageURL;

	public SkinPreviewPane() {
		super();
		setPreferredSize(new Dimension(136, 128));
		setImageURL(null);
	}

	public void setImageURL(URL url) {
		if (url == null) {
			try {
				imageURL = getClass().getResource("/launcher_mcskin.png");
				bim = ImageIO.read(imageURL);
				repaint();
			} catch (IOException e1) {
				LaunchCrashReporter.crash(e1	);
			}
			return;
		}
		try {
			imageURL = url;
			bim = ImageIO.read(imageURL);
			repaint();
		} catch (IOException e) {
			try {
				imageURL = getClass().getResource("/launcher_mcskin.png");
				bim = ImageIO.read(imageURL);
				repaint();
			} catch (IOException e1) {
				LaunchCrashReporter.crash(e1);
			}
		}
	}
	
	public URL getImageURL() {
		return imageURL;
	}
	
	public void drawSkinImageWithHQSupport(Graphics g, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4) {
		float baseSizeX = 64f;
		float baseSizeY = 32f;
		float xs = bim.getWidth() / baseSizeX;
		float ys = bim.getHeight() / baseSizeY;
		g.drawImage(bim, x1, y1, x2, y2, (int) (x3 * xs), (int) (y3 * ys), (int) (x4 * xs), (int) (y4 * ys), this);
	}

	@Override
	protected void paintComponent(Graphics _g) {
		super.paintComponent(_g);
		Graphics2D g = (Graphics2D) _g;
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
		
		drawSkinImageWithHQSupport(g, 16, 0, 48, 32, 8, 8, 16, 16);
		drawSkinImageWithHQSupport(g, 16, 32, 48, 80, 20, 20, 28, 32);
		drawSkinImageWithHQSupport(g, 0, 32, 16, 80, 44, 20, 48, 32);
		drawSkinImageWithHQSupport(g, 48, 32, 64, 80, 48, 20, 44, 32);
		drawSkinImageWithHQSupport(g, 16, 80, 32, 128, 4, 20, 8, 32);
		drawSkinImageWithHQSupport(g, 32, 80, 48, 128, 8, 20, 4, 32);

		drawSkinImageWithHQSupport(g, 72 + 16, 0, 72 + 48, 32, 24, 8, 32, 16);
		drawSkinImageWithHQSupport(g, 72 + 16, 32, 72 + 48, 80, 32, 20, 40, 32);
		drawSkinImageWithHQSupport(g, 72 + 0, 32, 72 + 16, 80, 56, 20, 52, 32);
		drawSkinImageWithHQSupport(g, 72 + 48, 32, 72 + 64, 80, 52, 20, 56, 32);
		drawSkinImageWithHQSupport(g, 72 + 16, 80, 72 + 32, 128, 16, 20, 12, 32);
		drawSkinImageWithHQSupport(g, 72 + 32, 80, 72 + 48, 128, 12, 20, 16, 32);
	}

}

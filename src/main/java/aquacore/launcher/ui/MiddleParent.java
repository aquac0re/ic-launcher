package aquacore.launcher.ui;

import java.awt.Component;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

public class MiddleParent extends JPanel {
	private static final long serialVersionUID = 1L;

	public MiddleParent(Component child) {
		setOpaque(false);
		setLayout(new MigLayout("", ":push[]:push", ":push[]:push"));
		
		add(child, "cell 0 0,grow");
		
	}

}

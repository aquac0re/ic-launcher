package aquacore.launcher.ui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class BgPane extends JPanel implements ComponentListener {

	private static final long serialVersionUID = 1L;
	private BufferedImage bg;

	public BgPane() {
		super(null);

		try {
			bg = (BufferedImage) ImageIO.read(getClass().getResource("/bg.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		addComponentListener(this);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		((Graphics2D) g).setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);

		int width = getWidth() - 1;
		int height = getHeight() - 1;

		double scaleFactor = getScaleFactorToFill(new Dimension(bg.getWidth(), bg.getHeight()), getSize());

		int scaleWidth = (int) Math.round(bg.getWidth() * scaleFactor);
		int scaleHeight = (int) Math.round(bg.getHeight() * scaleFactor);

		int x = (width - scaleWidth) / 2;
		int y = (height - scaleHeight) / 2;

		g.drawImage(bg, x, y, scaleWidth, scaleHeight, 0, 0, bg.getWidth(), bg.getHeight(), this);
	}

	public static double getScaleFactor(int iMasterSize, int iTargetSize) {

		return (double) iTargetSize / (double) iMasterSize;

	}

	public static double getScaleFactorToFill(Dimension masterSize, Dimension targetSize) {
		double dScaleWidth = getScaleFactor(masterSize.width, targetSize.width);
		double dScaleHeight = getScaleFactor(masterSize.height, targetSize.height);

		double dScale = Math.max(dScaleHeight, dScaleWidth);

		return dScale;
	}

	@Override
	public void componentResized(ComponentEvent e) {
		repaint();
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

}

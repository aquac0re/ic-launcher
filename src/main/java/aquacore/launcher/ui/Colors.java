package aquacore.launcher.ui;

public class Colors {

	public static int color_bg = 0xfdffff;
	public static int color_bg_d = 0xedefef;

	public static int color_dbg = 0xdddfdf;
	public static int color_dbg_d = 0xcdcfcf;

	public static int color_accent = 0x00A698;
	public static int color_accent_d = 0x009688;

	public static int color_daccent = 0x008678;
	public static int color_daccent_d = 0x007668;

	public static int color_titlebar = 0x009688;
	public static int color_error = 0xF8333C;
	public static int color_error_d = 0xC42930;
	public static int color_success = 0x44AF69;
	public static int color_success_d = 0x389157;

}

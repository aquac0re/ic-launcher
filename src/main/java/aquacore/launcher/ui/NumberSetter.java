package aquacore.launcher.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JComponent;
import javax.swing.JLabel;

public class NumberSetter extends JComponent {
	private static final long serialVersionUID = 1L;

	private int minNumber;
	private int maxNumber;
	private int currentNumber;

	private JLabel displayLabel;

	public NumberSetter(int min, int max, JLabel displayLabel) {
		this.minNumber = min;
		this.maxNumber = max;
		this.currentNumber = min;
		this.displayLabel = displayLabel;

		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				update(e);
			}
		});

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				update(e);
			}
		});
	}

	public void setCurrentNumber(int currentNumber) {
		this.currentNumber = currentNumber;
		updateText();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		// bg
		g.setColor(new Color(Colors.color_bg_d));
		g.fillRect(0, getHeight() / 2 - 2, getWidth(), 4);

		// selectedPart
		g.setColor(new Color(Colors.color_dbg_d));
		final int w = (int) (((float) currentNumber / (float) maxNumber) * getWidth());
		g.fillRect(0, getHeight() / 2 - 2, w, 4);

		// picker
		g.setColor(new Color(Colors.color_accent));
		g.fillRect(w - 4, getHeight() / 2 - 2, 4, 4);

		updateText();
	}

	public int getNumber() {
		return currentNumber;
	}

	private void update(MouseEvent e) {
		final float value = ((float) e.getX() / (float) getWidth()) * (maxNumber);
		currentNumber = (int) value;

		if (currentNumber < minNumber)
			currentNumber = minNumber;
		if (currentNumber >= maxNumber)
			currentNumber = maxNumber;

		updateText();
		repaint();
	}

	private void updateText() {
		displayLabel.setText("Всего: " + currentNumber + " МБ");
	}
}

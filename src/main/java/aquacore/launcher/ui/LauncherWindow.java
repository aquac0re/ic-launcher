package aquacore.launcher.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Stack;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;

import aquacore.launcher.applet.AppletWrapper;
import aquacore.launcher.launch.ThreadCryptoSetup;
import aquacore.launcher.ui.AquaCoreRootPaneUI.IColorHandler;
import aquacore.launcher.util.BypassExit;
import aquacore.launcher.util.Config;
import aquacore.launcher.util.LauncherProperties;

public class LauncherWindow extends JFrame implements WindowStateListener {
	private static final long serialVersionUID = 1L;

	public static LauncherWindow instance;
	
	public AppletWrapper appletWrapper = new AppletWrapper();

	private BgPane bgPane;
	private Component currentPane;
	private JLayeredPane layeredPane;

	private static interface IBackLayer {
		public Component getComponent();

		public void onBack();
	}

	private Stack<IBackLayer> backLayers = new Stack<>();

	public void pushBackLayer(final Component component) {
		backLayers.push(new IBackLayer() {

			@Override
			public void onBack() {

			}

			@Override
			public Component getComponent() {
				return component;
			}
		});
	}

	public void pushBackLayerAndSwitchTo(final Component component) {
		pushBackLayer(currentPane);
		setCurrentPane(component);
	}

	public void pushBackLayerAndSwitchTo(final Component component, final Component backComponent) {
		pushBackLayer(backComponent);
		setCurrentPane(component);
	}

	public void back() {
		IBackLayer l = backLayers.pop();
		setCurrentPane(l.getComponent());
		l.onBack();
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LauncherWindow frame = new LauncherWindow();
					frame.setVisible(true);
					LoadingPane.instance.setIndeterminate(true);
					LoadingPane.instance.setTitle("Инициализация...");
					frame.setCurrentPane(LoadingPane.renderInstance);
					new ThreadCryptoSetup().start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LauncherWindow() {
		instance = this;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setMinimumSize(new Dimension(925, 530));
		setSize();

		setUndecorated(true);
		setDefaultLookAndFeelDecorated(false);
		getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
		
		if ((getExtendedState() & MAXIMIZED_BOTH) == 0) {
			getRootPane()
					.setBorder(BorderFactory
							.createMatteBorder(0, 3, 3, 3,
									new Color(Integer.valueOf(LauncherProperties.instance.getProperty("titleColor")))));
		} else {
			getRootPane().setBorder(BorderFactory.createEmptyBorder());
		}

		AquaCoreRootPaneUI rpui = new AquaCoreRootPaneUI();
		rpui.setColorHandler(new IColorHandler() {

			@Override
			public void changeColor(Color color) {

				if ((getExtendedState() & MAXIMIZED_BOTH) == 0) {
					getRootPane().setBorder(BorderFactory.createMatteBorder(0, 3, 3, 3, color));
				} else {
					getRootPane().setBorder(BorderFactory.createEmptyBorder());
				}
				LauncherProperties.instance.setProperty("titleColor", String.valueOf(color.getRGB()));
				LauncherProperties.instance.store();

			}
		});
		getRootPane().setUI(rpui);
		rpui.setColorHandler(new Color(Integer.valueOf(LauncherProperties.instance.getProperty("titleColor"))));

		try {
			setIconImage(ImageIO.read(getClass().getResource("/server3_copy.png")));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		setTitle("AquaC0RE Launcher");

		layeredPane = new JLayeredPane();

		layeredPane.add(bgPane = new BgPane(), (Integer) 1);

		layeredPane.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				super.componentResized(e);
				updatePaneSizes();
			}
		});
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				onClose();
			}
		});
		
		addWindowStateListener(this);

		setContentPane(layeredPane);
	}

	private void updatePaneSizes() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				bgPane.setSize(layeredPane.getSize());
				if (currentPane != null) {
					currentPane.setSize(layeredPane.getSize());
				}
				revalidate();
			}
		});
	}

	public synchronized void setCurrentPane(final Component newPane) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (currentPane != null) {
					layeredPane.remove(currentPane);
				}
				layeredPane.add(currentPane = newPane, (Integer) 2);
				updatePaneSizes();
			}
		});
	}


	@SuppressWarnings("restriction")
	private void setSize() {
		boolean c = false;
		File file = null;
		try {
			file = new File(Config.getClientDir(), "screenData.txt");
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
			return;
		}

		c = file.exists();

		if (c) {
			try (BufferedReader r = new BufferedReader(new FileReader(file))) {
				String[] line = r.readLine().split(" ");

				setSize(Integer.parseInt(line[0].trim()), Integer.parseInt(line[1].trim()));
				setLocation(Integer.parseInt(line[2].trim()), Integer.parseInt(line[3].trim()));
				if (line[4].trim().equals("max")) {
					setVisible(true);
					setExtendedState(getExtendedState() | MAXIMIZED_BOTH);

					setVisible(false);
					sun.awt.AWTAccessor.getComponentAccessor().setPeer(this, null); // :'( SUN ONLY BUT THAT DOESNT
																					// MATTER
				}

				r.close();
			} catch (Exception e) {
				e.printStackTrace();
				setSize(925, 530);
				setLocationRelativeTo(null);
			}
		} else {
			setSize(925, 530);
			setLocationRelativeTo(null);
		}
		setMinimumSize(new Dimension(925, 530));
	}

	private void onClose() {
		try {
			File file = new File(Config.getClientDir(), "screenData.txt");
			if (!file.exists()) {
				file.createNewFile();
			}

			boolean maximized = (getExtendedState() & MAXIMIZED_BOTH) != 0;
			try (BufferedWriter w = new BufferedWriter(new FileWriter(file))) {
				w
						.write(getWidth() + " " + getHeight() + " " + getX() + " " + getY() + " "
								+ (maximized ? "max" : "norm"));
				w.close();
			}
		} catch (IOException | URISyntaxException e1) {
			e1.printStackTrace();
		}

		BypassExit.exit(0);
		System.exit(0);
	}
	

	@Override
	public void windowStateChanged(WindowEvent e) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				if ((getExtendedState() & MAXIMIZED_BOTH) == 0) {
					getRootPane()
							.setBorder(BorderFactory
									.createMatteBorder(0, 3, 3, 3, new Color(
											Integer.valueOf(LauncherProperties.instance.getProperty("titleColor")))));
				} else {
					getRootPane().setBorder(BorderFactory.createEmptyBorder());
				}
			}
		});
	}
}

package aquacore.launcher.ui;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

public class LoginMiddleParent extends JPanel {
	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private AquaCoreFlatButton aquaCoreFlatButton;
	private AquaCoreFlatButton aquaCoreFlatButton_1;

	public LoginMiddleParent(Component child1, Component child2) {
		setOpaque(false);
		setLayout(new MigLayout("", ":push[growprio 1,grow,sg tabs][growprio 1,grow,sg tabs]:push", ":push[30][]:push"));
		
		aquaCoreFlatButton = new AquaCoreFlatButton("Вход", 1);
		aquaCoreFlatButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)panel.getLayout()).show(panel, "login");
				aquaCoreFlatButton.changeState(1);
				aquaCoreFlatButton_1.changeState(0);
			}
		});
		add(aquaCoreFlatButton, "cell 0 0,grow");
		
		aquaCoreFlatButton_1 = new AquaCoreFlatButton("Регистрация");
		aquaCoreFlatButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)panel.getLayout()).show(panel, "register");
				aquaCoreFlatButton.changeState(0);
				aquaCoreFlatButton_1.changeState(1);
				
			}
		});
		add(aquaCoreFlatButton_1, "cell 1 0,grow");
		
		panel = new JPanel();
		panel.setLayout(new CardLayout());
		panel.add(LoginPane.instance, "login");
		panel.add(RegisterPanel.instance, "register");
		add(panel, "cell 0 1,span");
		
	}
}

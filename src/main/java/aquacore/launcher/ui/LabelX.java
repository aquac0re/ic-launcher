package aquacore.launcher.ui;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.io.IOException;

import javax.swing.JLabel;

public class LabelX extends JLabel {

	private static final long serialVersionUID = 1L;
	private static Font labelFont;
	private boolean isTitle;
	
	public LabelX(String title) {
		this(title, false);
	}
	
	static {
		try {
			labelFont = Font.createFont(Font.TRUETYPE_FONT, LabelX.class.getResourceAsStream("/Comfortaa-Regular.ttf"));
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
			labelFont = new Font("Arial", Font.PLAIN, 14);
		}
	}

	public LabelX(String title, boolean isTitle) {
		super(title);

		setOpaque(false);
		
		this.isTitle = isTitle;
	}

	@Override
	protected void paintComponent(Graphics g) {
		g.setFont(labelFont.deriveFont(isTitle ? (getHeight() / 1.8f) : 14f));
		super.paintComponent(g);
	}
}

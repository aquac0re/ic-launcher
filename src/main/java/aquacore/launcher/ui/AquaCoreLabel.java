package aquacore.launcher.ui;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.io.IOException;

import javax.swing.JLabel;

public class AquaCoreLabel extends JLabel {

	private static final long serialVersionUID = 1L;
	private Font labelFont;
	private boolean isTitle;
	
	public AquaCoreLabel(String title) {
		this(title, false);
	}

	public AquaCoreLabel(String title, boolean isTitle) {
		super(title);

		setOpaque(false);

		try {
			labelFont = Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("/Comfortaa-Regular.ttf"));
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
			labelFont = new Font("Arial", Font.PLAIN, (int) (getHeight() / 1.8));
		}
		
		this.isTitle = isTitle;
	}

	@Override
	protected void paintComponent(Graphics g) {
		g.setFont(labelFont.deriveFont(isTitle ? (getHeight() / 1.8f) : 14f));
		super.paintComponent(g);
	}
}

package aquacore.launcher.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import aquacore.launcher.util.Utils;
import net.miginfocom.swing.MigLayout;

public class RegisterPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public static final RegisterPanel instance = new RegisterPanel();
	
	public RegisterPanel() {
		setBackground(new Color(255, 255, 255));
		setPreferredSize(new Dimension(405, 435));
		setMaximumSize(new Dimension(405, 435));
		setMinimumSize(new Dimension(405, 435));
		setLayout(new MigLayout("", ":push[200]:push",
				":push[30]0[30]10[30]0[30]10[30]0[30]0[35]0[30]20[30]20[30]:push"));

		AquaCoreLabel loginLabel = new AquaCoreLabel("Логин:");
		add(loginLabel, "cell 0 0,grow");
		loginLabel.setForeground(new Color(Colors.color_daccent));

		final AquaCoreTextField loginField = new AquaCoreTextField(loginLabel);
		add(loginField, "cell 0 1,grow");

		AquaCoreLabel passLabel = new AquaCoreLabel("Пароль:");
		add(passLabel, "cell 0 2,grow");
		passLabel.setForeground(new Color(Colors.color_daccent));

		final AquaCorePassField passField = new AquaCorePassField(passLabel);
		add(passField, "cell 0 3,grow");

		AquaCoreLabel aquaCoreLabel_1 = new AquaCoreLabel("Повторите пароль:");
		add(aquaCoreLabel_1, "cell 0 4,grow");
		aquaCoreLabel_1.setForeground(new Color(Colors.color_daccent));

		final AquaCorePassField passField2 = new AquaCorePassField(passLabel);
		add(passField2, "cell 0 5,grow");

		final AquaCoreLabel r_error = new AquaCoreLabel("");
		add(r_error, "cell 0 6,grow");

		AquaCoreFlatButton registerButton = new AquaCoreFlatButton(1);
		registerButton.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				if (!passField.getText().equals(passField2.getText())) {
					r_error.setForeground(new Color(Colors.color_error));
					r_error.setText("Пароли не совпадают");
					r_error.setVisible(true);
				} else {
					String res = Utils.register(loginField.getText(), passField.getText());
					if (res != null) {
						r_error.setForeground(new Color(Colors.color_error));
						r_error.setText(res);
						r_error.setVisible(true);
					} else {
						r_error.setForeground(new Color(Colors.color_success));
						r_error.setText("Вы успешно зарегистрировались");
						r_error.setVisible(true);
					}
				}
			}
		});
		registerButton.setText("Зарег-ться");
		add(registerButton, "cell 0 7,grow");
	}

}

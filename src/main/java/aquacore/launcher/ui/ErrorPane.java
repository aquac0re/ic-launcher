package aquacore.launcher.ui;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.JTextArea;

import net.miginfocom.swing.MigLayout;

public class ErrorPane extends JPanel {
	private static final long serialVersionUID = 1L;
	
	public static final ErrorPane instance = new ErrorPane();
	public static final Component renderInstance = new MiddleParent(instance);

	private JTextArea label;

	/**
	 * Create the panel.
	 */
	public ErrorPane() {
		setBackground(Color.WHITE);
		setLayout(new MigLayout("", "25[400]25", "25[]25"));
		
		label = new JTextArea();
		label.setEditable(false);
		label.setOpaque(false);
		label.setBackground(null);
		label.setBorder(null);
		label.setLineWrap(true);
		label.setWrapStyleWord(true);
		label.setForeground(new Color(Colors.color_accent));
		add(label, "cell 0 0,grow");

	}
	
	public void setText(String text) {
		label.setText(text);
	}

	public void setTextColor(Color color) {
		label.setForeground(color);
	}
}

package aquacore.launcher.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.io.IOException;

import javax.swing.JButton;

import org.jdesktop.animation.timing.Animator;
import org.jdesktop.animation.timing.TimingTargetAdapter;

public class AquaCoreFlatButton extends JButton implements ToColorTimingTarget.IColorSettable {

	private static final long serialVersionUID = 1L;
	private int state;
	private Font buttonFont;
	private Animator currentAnimator;

	// private static final int bottom_offset = 4;

	// private boolean hovered = false, pressed = false;

	public AquaCoreFlatButton() {
		this(0);
	}

	public AquaCoreFlatButton(int state) {
		this("", state);
	}

	public AquaCoreFlatButton(String text) {
		this(text, 0);
	}

	public AquaCoreFlatButton(String text, int state) {
		super(text);
		setBorderPainted(false);
		setFocusPainted(false);

		setOpaque(false);

		try {
			buttonFont = Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("/Comfortaa-Regular.ttf"));
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();

			buttonFont = new Font("Arial", Font.PLAIN, (int) (getHeight() / 1.7));
		}

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				super.mouseEntered(e);
				updateColor();
			}

			@Override
			public void mouseExited(MouseEvent e) {
				super.mouseExited(e);
				updateColor();
			}

			@Override
			public void mousePressed(MouseEvent e) {
				super.mousePressed(e);
				updateColor();
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				super.mouseReleased(e);
				updateColor();
			}
		});

		this.state = state;
		this.color = getUpdateColor();
	}

	private Color color;

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		/*
		 * if (!getModel().isPressed()) { if (state == 0) { g.setColor( new
		 * Color(getModel().isRollover() ? NewLauncherWindow.color_bg :
		 * NewLauncherWindow.color_bg_d)); } else { g.setColor(new Color(
		 * getModel().isRollover() ? NewLauncherWindow.color_accent :
		 * NewLauncherWindow.color_accent_d)); }
		 * 
		 * g.fillRect(0, 0, getWidth(), getHeight()); } else { g.setColor(new
		 * Color(state == 0 ? (getModel().isRollover() ? NewLauncherWindow.color_dbg :
		 * NewLauncherWindow.color_dbg_d) : (getModel().isRollover() ?
		 * NewLauncherWindow.color_daccent : NewLauncherWindow.color_daccent_d)));
		 * g.fillRect(0, 0, getWidth(), getHeight()); }
		 */

		g.setColor(color);

		g.fillRect(0, 0, getWidth(), getHeight());

		g.setColor(new Color(state == 0 ? Colors.color_accent : Colors.color_bg));

		Font font = buttonFont.deriveFont((float) (getHeight() * 0.6));
		Rectangle2D rect = font.getStringBounds(getText(), ((Graphics2D) g).getFontRenderContext());
		FontMetrics fm = g.getFontMetrics(font);

		g.setFont(font);
		g
				.drawString(getText(), (int) (getWidth() / 2 - rect.getWidth() / 2),
						(getHeight() - fm.getHeight()) / 2 + fm.getAscent());
	}

	public void changeState(final int newState) {
		state = newState;
		updateColor();
	}

	private Color getUpdateColor() {
		if (!getModel().isPressed()) {
			if (state == 0) {
				return (new Color(getModel().isRollover() ? Colors.color_bg : Colors.color_bg_d));
			} else {
				return (new Color(
						getModel().isRollover() ? Colors.color_accent : Colors.color_accent_d));
			}
		} else {
			return (new Color(state == 0
					? (getModel().isRollover() ? Colors.color_dbg : Colors.color_dbg_d)
					: (getModel().isRollover() ? Colors.color_daccent : Colors.color_daccent_d)));
		}
	}

	private void updateColor() {
		if (currentAnimator != null) {
			currentAnimator.stop();
		}
		currentAnimator = new Animator(200, new ToColorTimingTarget(color, getUpdateColor(), this));
		currentAnimator.addTarget(new TimingTargetAdapter() {
			@Override
			public void end() {
				currentAnimator = null;
			}
		});
		currentAnimator.start();
	}

	@Override
	public void setICSColor(Color color) {
		this.color = color;
		repaint();
	}
}

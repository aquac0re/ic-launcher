package aquacore.launcher.ui;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.Border;

public class AquaCoreTextField extends JTextField {

	private Border textBorder;
	private Border textBorderSelected;

	private static final long serialVersionUID = 1L;

	public AquaCoreTextField(final JLabel labelFor) {
		setForeground(new Color(Colors.color_daccent));

		setOpaque(false);

		textBorder = BorderFactory
				.createCompoundBorder(
						BorderFactory.createMatteBorder(1, 1, 3, 1, new Color(Colors.color_daccent)),
						BorderFactory.createEmptyBorder(5, 5, 5, 5));
		textBorderSelected = BorderFactory
				.createCompoundBorder(
						BorderFactory.createMatteBorder(1, 1, 3, 1, new Color(Colors.color_accent)),
						BorderFactory.createEmptyBorder(5, 5, 5, 5));
		setBorder(textBorder);

		addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				setForeground(new Color(Colors.color_daccent));
				labelFor.setForeground(new Color(Colors.color_daccent));
				setBorder(textBorder);
				getRootPane().repaint();
			}

			@Override
			public void focusGained(FocusEvent e) {
				setForeground(new Color(Colors.color_accent));
				labelFor.setForeground(new Color(Colors.color_accent));
				setBorder(textBorderSelected);
				getRootPane().repaint();
			}
		});
	}
}

package aquacore.launcher.ui;

import java.awt.Color;

import org.jdesktop.animation.timing.TimingTarget;

public class ToColorTimingTarget implements TimingTarget {

	private Color beginColor;
	private Color endColor;
	private IColorSettable settable;

	public ToColorTimingTarget(Color beginColor, Color endColor, IColorSettable settable) {
		super();
		this.beginColor = beginColor;
		this.endColor = endColor;
		this.settable = settable;
	}

	public static interface IColorSettable {
		public void setICSColor(Color color);
	}

	@Override
	public void begin() {
		// TODO Auto-generated method stub

	}

	@Override
	public void end() {
		// TODO Auto-generated method stub

	}

	@Override
	public void repeat() {
		// TODO Auto-generated method stub

	}

	@Override
	public void timingEvent(float part) {
		int r = beginColor.getRed();
		int g = beginColor.getGreen();
		int b = beginColor.getBlue();
		int a = beginColor.getAlpha();
		r += (endColor.getRed() - r) * part;
		g += (endColor.getGreen() - g) * part;
		b += (endColor.getBlue() - b) * part;
		a += (endColor.getAlpha() - a) * part;
		settable.setICSColor(new Color(r, g, b, a));
	}

}

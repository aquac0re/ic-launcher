package aquacore.launcher.tweaker;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import cpw.mods.fml.relauncher.IFMLLoadingPlugin;
import cpw.mods.fml.relauncher.ILibrarySet;
import net.minecraft.launchwrapper.LogWrapper;

public class LibraryFixerCoremod implements IFMLLoadingPlugin {
	@Override
	public String[] getASMTransformerClass() {
		return new String[0];
	}

	@Override
	public void injectData(Map<String, Object> data) {
	}

	@Override
	public String getSetupClass() {
		return null;
	}

	@Override
	public String getModContainerClass() {
		return null;
	}

	@Override
	public String[] getLibraryRequestClass() {
		try {
			Field field = Class.forName("cpw.mods.fml.relauncher.RelaunchLibraryManager").getDeclaredField("libraries");
			field.setAccessible(true);
			@SuppressWarnings("unchecked")
			List<ILibrarySet> libraries = (List<ILibrarySet>) field.get(null);
			ILibrarySet[] libs = libraries.toArray(new ILibrarySet[libraries.size()]);
			for (ILibrarySet lib : libs) {
				if (lib.getClass().getCanonicalName().equals("cpw.mods.fml.relauncher.CoreFMLLibraries")) {
					LogWrapper.info("[LegacyFixer] Removing CoreFMLLibraries: " + lib);
					libraries.remove(lib);
				} else if (lib.getRootURL().contains("files.minecraftforge.net/fmllibs/")) {
					LogWrapper.info("[LegacyFixer] Removing fmllibs reference: " + lib);
					libraries.remove(lib);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {};
	}

	@Override
	public String getAccessTransformerClass() {
		// TODO Auto-generated method stub
		return null;
	}
}

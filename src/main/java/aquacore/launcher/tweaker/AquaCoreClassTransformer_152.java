package aquacore.launcher.tweaker;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodNode;

import aquacore.launcher.nocl.Java9Utils;
import cpw.mods.fml.relauncher.IClassTransformer;
import net.minecraft.launchwrapper.LogWrapper;

public class AquaCoreClassTransformer_152 extends AquaCoreClassTransformer implements IClassTransformer {
	@Override
	public byte[] transform(String name, String transformedName, byte[] bytes) {
		if (!name.equals("cpw.mods.fml.relauncher.RelaunchLibraryManager")
				&& !name.equals("net.minecraftforge.common.EnumHelper")) {
			return bytes;
		}

		LogWrapper.info("[Launcher] Transforming " + name);
		final ClassNode classNode = readClassNode(bytes);

		if (name.equals("aus")) {
			MethodNode method = getMethodFromClass(classNode, "run", "()V");
			if (method == null) {
				LogWrapper.warning("[Launcher] Transforming aus: run doesn't exist WTF?");
				return bytes;
			}
			method.instructions.insertBefore(method.instructions.getFirst(), new InsnNode(Opcodes.RETURN));
		} else if (name.equals("cpw.mods.fml.relauncher.RelaunchLibraryManager")) {
			MethodNode method = getMethodFromClass(classNode, "downloadFile");
			if (method == null) {
				LogWrapper.warning("[Launcher] Transforming RelaunchLibraryManager: downloadFile doesn't exist WTF?");
				return bytes;
			}
			method.instructions.insertBefore(method.instructions.getFirst(), new InsnNode(Opcodes.RETURN));
		} else if (Java9Utils.checkIfNewJava() && name.equals("net.minecraftforge.common.EnumHelper")) {
			MethodNode method = getMethodFromClass(classNode, "setup");
			if (method == null) {
				System.out.println("[Launcher] Transforming EnumHelper: setup doesn't exist WTF?");
				return bytes;
			}
			for (int i = 0; i < method.instructions.size(); i++) {
				AbstractInsnNode insn = (AbstractInsnNode) method.instructions.get(i);
				if (insn instanceof LdcInsnNode) {
					LdcInsnNode ldc = (LdcInsnNode) insn;
					if (ldc.cst instanceof String) {
						String s = (String) ldc.cst;
						String newString = s.replace("sun.reflect", "jdk.internal.reflect");
						System.out.println("[Launcher] substituting: " + s + " -> " + newString);
						ldc.cst = newString;
					}
				}
			}
		}
		byte[] newbytes = writeClassNode(classNode);
		return newbytes;
	}
}

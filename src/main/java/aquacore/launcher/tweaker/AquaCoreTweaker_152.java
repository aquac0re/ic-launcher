package aquacore.launcher.tweaker;

import java.io.File;
import java.util.List;

import net.minecraft.launchwrapper.ITweaker;
import net.minecraft.launchwrapper.LaunchClassLoader;

public class AquaCoreTweaker_152 implements ITweaker {

	private List<String> args;

	@Override
	public void acceptOptions(List<String> args, File gameDir, File assetsDir, String profile) {
		String old = System.getProperty("fml.coreMods.load", "");
		String changed = AquaCoreLoadingPlugin_152.class.getCanonicalName();
		if (!old.isEmpty()) {
			changed = old + "," + changed;
		}
		System.setProperty("fml.coreMods.load", changed);
		this.args = args;
	}

	@Override
	public void injectIntoClassLoader(LaunchClassLoader classLoader) {
		classLoader.addClassLoaderExclusion("aquacore.launcher.NewLauncherWindow");
		classLoader.addClassLoaderExclusion("aquacore.launcher.applet");
		classLoader.registerTransformer(AquaCoreClassTransformer_152_1.class.getCanonicalName());
	}

	@Override
	public String getLaunchTarget() {
		return "net.minecraft.client.MinecraftApplet";
	}

	@Override
	public String[] getLaunchArguments() {
		return args.toArray(new String[args.size()]);
	}

}

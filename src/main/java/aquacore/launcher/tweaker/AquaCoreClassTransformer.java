package aquacore.launcher.tweaker;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodNode;

import aquacore.launcher.nocl.Java9Utils;
import aquacore.launcher.ui.LauncherWindow;
import net.minecraft.launchwrapper.IClassTransformer;
import net.minecraft.launchwrapper.Launch;
import net.minecraft.launchwrapper.LogWrapper;

public class AquaCoreClassTransformer implements IClassTransformer {

	@Override
	public byte[] transform(String name, String transformedName, byte[] bytes) {
		// System.out.println(name);
		if (!name.equals("net.minecraftforge.client.ForgeHooksClient")
				&& !name.equals("net.minecraftforge.common.util.EnumHelper")/* && !name.equals("blg") */) {
			return bytes;
		}
		LogWrapper.info("[Launcher] Transforming " + name);
		final ClassNode classNode = readClassNode(bytes);
		if (name.equals("net.minecraftforge.client.ForgeHooksClient")) {
			MethodNode createDisplayMethod = getMethodFromClass(classNode, "createDisplay");
			if (createDisplayMethod == null) {
				LogWrapper.warning("[Launcher] Transforming ForgeHooksClient: createDisplay doesn't exist WTF?");
				return bytes;
			}
			final MethodNode injectedMethod = createStaticCallMethodNode(
					"aquacore/launcher/tweaker/AquaCoreClassTransformer", "injectLWJGL", "()V", false);
			createDisplayMethod.instructions
					.insertBefore(createDisplayMethod.instructions.get(0), injectedMethod.instructions);
		} else if (Java9Utils.checkIfNewJava() && name.equals("net.minecraftforge.common.util.EnumHelper")) {
			MethodNode method = getMethodFromClass(classNode, "setup");
			if (method == null) {
				System.out.println("[Launcher] Transforming EnumHelper: setup doesn't exist WTF?");
				return bytes;
			}
			for (int i = 0; i < method.instructions.size(); i++) {
				AbstractInsnNode insn = (AbstractInsnNode) method.instructions.get(i);
				if (insn instanceof LdcInsnNode) {
					LdcInsnNode ldc = (LdcInsnNode) insn;
					if (ldc.cst instanceof String) {
						String s = (String) ldc.cst;
						String newString = s.replace("sun.reflect", "jdk.internal.reflect");
						System.out.println("[Launcher] substituting: " + s + " -> " + newString);
						ldc.cst = newString;
					}
				}
			}
		} /*
			 * else if (name.equals("blg")) { for (final MethodNode methodNode :
			 * classNode.methods) { if(methodNode.name.equals("a") &&
			 * methodNode.desc.equals("(Lbqx;Ljava/lang/String;)Lbpr;")) { System.out.
			 * println("[Launcher] Transforming blg.a(Lbqx;Ljava/lang/String;)Lbpr");
			 * for(int i = 0; i < methodNode.instructions.size(); i++) { AbstractInsnNode
			 * insn = methodNode.instructions.get(i); if(insn instanceof LdcInsnNode) {
			 * if(((LdcInsnNode)
			 * insn).cst.equals("http://skins.minecraft.net/MinecraftSkins/%s.png")) {
			 * System.out.
			 * println("[Launcher] Transforming blg.a(Lbqx;Ljava/lang/String;)Lbpr: LDC " +
			 * ((LdcInsnNode) insn).cst); ((LdcInsnNode) insn).cst = Config.launchServer +
			 * "/skins/%s.png"; } } } break; } } }
			 */
		return writeClassNode(classNode);
	}

	public static MethodNode createStaticCallMethodNode(String callClass, String callMethod, String callSignature,
			boolean noreturn) {

		final MethodNode injectedMethod = new MethodNode();
		final Label label = new Label();
		injectedMethod.visitLabel(label);
		injectedMethod.visitLineNumber(666, label);
		injectedMethod.visitMethodInsn(Opcodes.INVOKESTATIC, callClass, callMethod, callSignature, false);
		if (!noreturn)
			injectedMethod.visitInsn(Opcodes.RETURN);
		return injectedMethod;
	}

	public static MethodNode getMethodFromClass(ClassNode clazz, String methodName) {
		for (final MethodNode methodNode : clazz.methods) {
			if (methodName.equals(methodNode.name)) {
				return methodNode;
			}
		}
		return null;
	}

	public static MethodNode getMethodFromClass(ClassNode clazz, String methodName, String descriptor) {
		for (final MethodNode methodNode : clazz.methods) {
			if (methodName.equals(methodNode.name) && descriptor.equals(methodNode.desc)) {
				return methodNode;
			}
		}
		return null;
	}

	public static ClassNode readClassNode(byte[] bytes) {
		final ClassNode classNode = new ClassNode();
		final ClassReader classReader = new ClassReader(bytes);
		classReader.accept(classNode, ClassReader.EXPAND_FRAMES);
		return classNode;
	}

	public static ClassNode readClassNode2(byte[] bytes) {
		final ClassNode classNode = new ClassNode();
		final ClassReader classReader = new ClassReader(bytes);
		classReader.accept(classNode, ClassReader.SKIP_FRAMES);
		return classNode;
	}

	public static byte[] writeClassNode(ClassNode node) {
		final ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);
		node.accept(writer);
		return writer.toByteArray();
	}

	public static byte[] writeClassNode2(ClassNode node) {
		final ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES) {
			@Override
			protected String getCommonSuperClass(String type1, String type2) {
				Class<?> c, d;

				Class<?> relauncher;
				ClassLoader cl = Launch.classLoader;
				try {
					relauncher = Class.forName("cpw.mods.fml.relauncher.FMLRelauncher");
					Field field = relauncher.getDeclaredField("INSTANCE");
					field.setAccessible(true);
					Object instance = field.get(null);
					Field field1 = relauncher.getDeclaredField("classLoader");
					field1.setAccessible(true);
					cl = (ClassLoader) field1.get(instance);
				} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
						| NoSuchFieldException | SecurityException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				try {
					c = Class.forName(type1.replace('/', '.'), false, cl);
					d = Class.forName(type2.replace('/', '.'), false, cl);
				} catch (Exception e) {
					throw new RuntimeException(e.toString());
				}
				if (c.isAssignableFrom(d)) {
					return type1;
				}
				if (d.isAssignableFrom(c)) {
					return type2;
				}
				if (c.isInterface() || d.isInterface()) {
					return "java/lang/Object";
				} else {
					do {
						c = c.getSuperclass();
					} while (!c.isAssignableFrom(d));
					return c.getName().replace('.', '/');
				}

			}
		};
		node.accept(writer);
		return writer.toByteArray();
	}

	public static void injectLWJGL() throws Exception {
		Class<?> display;
		display = Class.forName("org.lwjgl.opengl.Display");

		System.out.println("[Launcher] injecting custom display");
		ImageIO.setUseCache(false);

		/*
		 * Method setParent = display.getMethod("setParent", Canvas.class);
		 * setParent.invoke(null, LauncherWindow.instance.gamePanel);
		 */
		display.getMethod("setResizable", boolean.class).invoke(null, true);
		display.getMethod("setTitle", String.class).invoke(null, "AquaC0RE Client");
		display
				.getMethod("setIcon", ByteBuffer[].class)
				.invoke(null,
						new Object[] { new ByteBuffer[] {
								loadIcon(LauncherWindow.class.getResourceAsStream("/server3_copy2.png")),
								loadIcon(LauncherWindow.class.getResourceAsStream("/server3_copy1.png")) } });

		// display.getMethod("setParent", Canvas.class).invoke(null,
		// NewLauncherWindow.INSTANCE.createAWTCanvas());

		display.getMethod("create").invoke(null);

		dismissLauncher();
	}

	public static void dismissLauncher() throws Exception {
		LauncherWindow.instance.setVisible(false);
	}

	private static ByteBuffer loadIcon(final InputStream iconFile) throws IOException {
		final BufferedImage icon = ImageIO.read(iconFile);

		final int[] rgb = icon.getRGB(0, 0, icon.getWidth(), icon.getHeight(), null, 0, icon.getWidth());

		final ByteBuffer buffer = ByteBuffer.allocate(4 * rgb.length);
		for (int color : rgb) {
			buffer.putInt(color << 8 | ((color >> 24) & 0xFF));
		}
		buffer.flip();
		return buffer;
	}
}

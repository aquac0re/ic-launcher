package aquacore.launcher.tweaker;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import cpw.mods.fml.relauncher.IFMLLoadingPlugin;
import cpw.mods.fml.relauncher.ILibrarySet;
import net.minecraft.launchwrapper.LogWrapper;

public class AquaCoreLoadingPlugin_152 implements IFMLLoadingPlugin {

	@SuppressWarnings("unchecked")
	@Override
	public String[] getLibraryRequestClass() {
		try {
			Field field = Class.forName("cpw.mods.fml.relauncher.RelaunchLibraryManager").getDeclaredField("libraries");
			field.setAccessible(true);
			List<ILibrarySet> libraries = (List<ILibrarySet>) field.get(null);
			ILibrarySet[] libs = libraries.toArray(new ILibrarySet[libraries.size()]);
			for (ILibrarySet lib : libs) {
				if (lib.getClass().getCanonicalName().equals("cpw.mods.fml.relauncher.CoreFMLLibraries")) {
					LogWrapper.info("[LegacyFixer] Removing CoreFMLLibraries: " + lib);
					libraries.remove(lib);
				} else if (lib.getRootURL().contains("files.minecraftforge.net/fmllibs/")) {
					LogWrapper.info("[LegacyFixer] Removing fmllibs reference: " + lib);
					libraries.remove(lib);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] {};
	}

	@Override
	public String[] getASMTransformerClass() {

		return new String[] { AquaCoreClassTransformer_152.class.getCanonicalName() };
	}

	@Override
	public String getModContainerClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSetupClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void injectData(Map<String, Object> data) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getAccessTransformerClass() {
		// TODO Auto-generated method stub
		return null;
	}

}

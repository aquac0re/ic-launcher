package aquacore.launcher.tweaker;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.MethodNode;

import net.minecraft.launchwrapper.LogWrapper;

public class AquaCoreClassTransformer_152_1 extends AquaCoreClassTransformer {
	@Override
	public byte[] transform(String name, String transformedName, byte[] bytes) {
		if (!name.equals("cpw.mods.fml.relauncher.RelaunchLibraryManager")) {
			return bytes;
		}

		LogWrapper.info("[Launcher] Transforming " + name);
		final ClassNode classNode = readClassNode(bytes);
		if (name.equals("cpw.mods.fml.relauncher.RelaunchLibraryManager")) {
			MethodNode method = getMethodFromClass(classNode, "downloadFile");
			if (method == null) {
				LogWrapper.warning("[Launcher] Transforming RelaunchLibraryManager: downloadFile doesn't exist WTF?");
				return bytes;
			}
			method.instructions.insertBefore(method.instructions.getFirst(), new InsnNode(Opcodes.RETURN));
		}
		byte[] newbytes = writeClassNode(classNode);
		return newbytes;
	}
}

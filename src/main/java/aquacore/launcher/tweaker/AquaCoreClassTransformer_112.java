package aquacore.launcher.tweaker;

import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import net.minecraft.launchwrapper.LogWrapper;

public class AquaCoreClassTransformer_112 extends AquaCoreClassTransformer {

	@Override
	public byte[] transform(String name, String transformedName, byte[] bytes) {
		if (!name.equals("bib")) {
			return bytes;
		}

		LogWrapper.info("[Launcher] Transforming " + name);
		final ClassNode classNode = readClassNode(bytes);
		if (name.equals("bib")) {
			MethodNode createDisplayMethod = getMethodFromClass(classNode, "at", "()V");
			if (createDisplayMethod == null) {
				LogWrapper.warning("[Launcher] Transforming Minecraft: createDisplay doesn't exist WTF?");
				return bytes;
			}

			LogWrapper.info("[Launcher] Transforming Minecraft.createDisplay()V");

			final MethodNode injectedMethod = createStaticCallMethodNode(
					"aquacore/launcher/tweaker/AquaCoreClassTransformer", "injectLWJGL", "()V", false);
			createDisplayMethod.instructions
					.insertBefore(createDisplayMethod.instructions.get(0), injectedMethod.instructions);
		}
		byte[] newbytes = writeClassNode(classNode);
		return newbytes;
	}

}

package aquacore.launcher.platform;

import java.io.File;
import java.nio.file.FileSystems;

import aquacore.launcher.util.OsUtils;

public final class PlatformUtils {
	private static final boolean isPosix = FileSystems.getDefault().supportedFileAttributeViews().contains("posix");
	
	private static PlatformUtils instance;
	
	private IPlatform impl;
	
	private PlatformUtils() throws UnsupportedPlatformException {
		if(isPosix) {
			impl = new POSIXPlatform();
		} else if(OsUtils.isWindows()) {
			impl = new Win32Platform();
		} else {
			throw new UnsupportedPlatformException();
		}
	}
	
	public static PlatformUtils get() throws UnsupportedPlatformException {
		if(instance == null) {
			instance = new PlatformUtils();
		}
		return instance;
	}
	
	public void setCWD(File file) {
		System.setProperty("user.dir", file.getAbsolutePath());
		impl.setCWD(file);
	}
}

package aquacore.launcher.platform;

import com.sun.jna.Library;

public interface POSIXPlatformNativeImpl extends Library {
	int chdir(String path);
}

package aquacore.launcher.platform;

import com.sun.jna.Library;

public interface Win32PlatformNativeImpl extends Library {
	boolean SetCurrentDirectory(String path);
}

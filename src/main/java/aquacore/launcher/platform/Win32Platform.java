package aquacore.launcher.platform;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;

import com.sun.jna.Native;
import com.sun.jna.win32.W32APIOptions;

class Win32Platform implements IPlatform {
	private final Win32PlatformNativeImpl impl;

	public Win32Platform() throws UnsupportedPlatformException {
		try {
			this.impl = Native.loadLibrary("KERNEL32", Win32PlatformNativeImpl.class, W32APIOptions.UNICODE_OPTIONS);
		} catch (UnsatisfiedLinkError e) {
			throw new UnsupportedPlatformException();
		}
	}

	@Override
	public void setCWD(File file) {
		File absFile = file.getAbsoluteFile();
		
		// NIO SUPPORT
		FileSystem fs = FileSystems.getDefault();
		try {
			Class<?> wfsc = Class.forName("sun.nio.fs.WindowsFileSystem");
			Class<?> wppc = Class.forName("sun.nio.fs.WindowsPathParser");
			Class<?> wpprc = Class.forName("sun.nio.fs.WindowsPathParser$Result");
			Method parseMethod = wppc.getDeclaredMethod("parse", String.class);
			parseMethod.setAccessible(true);
			Object res = parseMethod.invoke(null, absFile.getPath());
			Method pathMethod = wpprc.getDeclaredMethod("path");
			pathMethod.setAccessible(true);
			Method rootMethod = wpprc.getDeclaredMethod("root");
			rootMethod.setAccessible(true);
			String path = (String) pathMethod.invoke(res);
			String root = (String) rootMethod.invoke(res);
			Field modifiersField = Field.class.getDeclaredField("modifiers");
			modifiersField.setAccessible(true);

			Field pathField = wfsc.getDeclaredField("defaultDirectory");
			pathField.setAccessible(true);
			modifiersField.setInt(pathField, pathField.getModifiers() & ~Modifier.FINAL);
			pathField.set(fs, path);

			Field rootField = wfsc.getDeclaredField("defaultRoot");
			rootField.setAccessible(true);
			modifiersField.setInt(rootField, rootField.getModifiers() & ~Modifier.FINAL);
			rootField.set(fs, root);
		} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException | NoSuchFieldException e) {
			e.printStackTrace();
		}
		// NIO SUPPORT END
		
		impl.SetCurrentDirectory(absFile.getPath());
	}
}

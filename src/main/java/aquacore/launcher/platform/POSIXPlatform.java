package aquacore.launcher.platform;

import java.io.File;

import com.sun.jna.Native;

class POSIXPlatform implements IPlatform {
	private final POSIXPlatformNativeImpl impl;

	public POSIXPlatform() throws UnsupportedPlatformException {
		try {
			this.impl = Native.loadLibrary("c", POSIXPlatformNativeImpl.class);
		} catch (UnsatisfiedLinkError e) {
			throw new UnsupportedPlatformException();
		}
	}

	@Override
	public void setCWD(File file) {
		impl.chdir(file.getAbsolutePath());
	}
}

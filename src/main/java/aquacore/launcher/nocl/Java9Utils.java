package aquacore.launcher.nocl;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

import sun.misc.Unsafe;

@SuppressWarnings("restriction")
public class Java9Utils {

	@SuppressWarnings({ "unchecked" })
	public static URL[] getUrls(ClassLoader classLoader) {
		if (classLoader instanceof URLClassLoader) {
			return ((URLClassLoader) classLoader).getURLs();
		}

		// jdk9
		if (classLoader.getClass().getName().startsWith("jdk.internal.loader.ClassLoaders$")) {
			try {
				Field field = Unsafe.class.getDeclaredField("theUnsafe");
				field.setAccessible(true);
				Unsafe unsafe = (Unsafe) field.get(null);

				// jdk.internal.loader.ClassLoaders.AppClassLoader.ucp
				Field ucpField = classLoader.getClass().getDeclaredField("ucp");
				long ucpFieldOffset = unsafe.objectFieldOffset(ucpField);
				Object ucpObject = unsafe.getObject(classLoader, ucpFieldOffset);

				// jdk.internal.loader.URLClassPath.path
				Field pathField = ucpField.getType().getDeclaredField("path");
				long pathFieldOffset = unsafe.objectFieldOffset(pathField);
				ArrayList<URL> path = (ArrayList<URL>) unsafe.getObject(ucpObject, pathFieldOffset);

				return path.toArray(new URL[path.size()]);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	public static void addURL(URL url) throws IOException {
		ClassLoader sysloader = (ClassLoader) ClassLoader.getSystemClassLoader();
		if (sysloader instanceof URLClassLoader) {
			Class<URLClassLoader> sysclass = URLClassLoader.class;

			try {
				Method method = sysclass.getDeclaredMethod("addURL", new Class[] { URL.class });
				method.setAccessible(true);
				method.invoke(sysloader, new Object[] { url });
			} catch (Throwable t) {
				t.printStackTrace();
				throw new IOException("Error, could not add URL to system classloader");
			}
		} else if (sysloader.getClass().getName().startsWith("jdk.internal.loader.ClassLoaders$")) {
			try {
				Field field = Unsafe.class.getDeclaredField("theUnsafe");
				field.setAccessible(true);
				Unsafe unsafe = (Unsafe) field.get(null);

				// jdk.internal.loader.ClassLoaders.AppClassLoader.ucp
				Field ucpField = sysloader.getClass().getDeclaredField("ucp");
				long ucpFieldOffset = unsafe.objectFieldOffset(ucpField);
				Object ucpObject = unsafe.getObject(sysloader, ucpFieldOffset);

				Method addURLMethod = ucpObject.getClass().getMethod("addURL", URL.class);
				addURLMethod.invoke(ucpObject, url);
				// ucpObject.addURL(url);
			} catch (Throwable e) {
				e.printStackTrace();
				throw new IOException("Error, could not add URL to system classloader");
			}
		} else {
			throw new IOException("Error, could not add URL to system classloader");
		}
	}

	public static boolean checkIfNewJava() {
		float javaVersion = Float.parseFloat(System.getProperty("java.class.version"));
		if (javaVersion > 52.99f) {
			return true;
		}
		return false;
	}

}

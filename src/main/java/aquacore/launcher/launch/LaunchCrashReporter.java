package aquacore.launcher.launch;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import javax.swing.SwingUtilities;

import aquacore.launcher.ui.Colors;
import aquacore.launcher.ui.ErrorPane;
import aquacore.launcher.ui.LauncherWindow;

public class LaunchCrashReporter {
	public static void crash(Throwable e) {
		if (LauncherWindow.instance == null) {
			System.out.println("=== LAUNCHER CRASH REPORT ===");
			e.printStackTrace();
			System.exit(1);
		} else {
		    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		    try (PrintStream ps = new PrintStream(baos, true, "UTF-8")) {
		        e.printStackTrace(ps);
		    } catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}
		    
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					ErrorPane.instance.setText(new String(baos.toByteArray(), StandardCharsets.UTF_8));
					ErrorPane.instance.setTextColor(new Color(Colors.color_error));
					LauncherWindow.instance.setCurrentPane(ErrorPane.renderInstance);
				}
			});
		}
	}
}

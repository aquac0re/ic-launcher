package aquacore.launcher.launch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import aquacore.launcher.nocl.Java9Utils;
import aquacore.launcher.util.Config;
import aquacore.launcher.util.LauncherProperties;
import aquacore.launcher.util.Utils;

public class LauncherRelauncher {
	public static void relaunch() {
		System.out.println("Relaunching...");
		List<String> jvmLaunchCmd = new ArrayList<>();
		jvmLaunchCmd.add(Utils.getJavaExecutable().getAbsolutePath());
		jvmLaunchCmd.add("-XX:+UseConcMarkSweepGC");
		// jvmLaunchCmd.add("-XX:+AggressiveOpts");
		jvmLaunchCmd.add("-XX:+UseAdaptiveSizePolicy");
		jvmLaunchCmd.add("-XX:+DisableAttachMechanism");
		jvmLaunchCmd.add("-Dfile.encoding=UTF-8");

		if (Java9Utils.checkIfNewJava()) {
			System.out.println("adding java 9 options");
			jvmLaunchCmd.add("--illegal-access=warn");
			jvmLaunchCmd.add("--add-opens");
			jvmLaunchCmd.add("java.base/java.lang=ALL-UNNAMED");
			jvmLaunchCmd.add("--add-opens");
			jvmLaunchCmd.add("java.base/jdk.internal.loader=ALL-UNNAMED");
			jvmLaunchCmd.add("--add-opens");
			jvmLaunchCmd.add("java.base/sun.reflect=ALL-UNNAMED");
			jvmLaunchCmd.add("--add-opens");
			jvmLaunchCmd.add("java.base/java.lang.reflect=ALL-UNNAMED");
			jvmLaunchCmd.add("--add-opens");
			jvmLaunchCmd.add("java.base/jdk.internal.reflect=ALL-UNNAMED");

		}

		jvmLaunchCmd.add("-Xmx" + LauncherProperties.instance.getProperty("xmx") + "M");
		jvmLaunchCmd.add("-cp");
		ClassLoader cl = ClassLoader.getSystemClassLoader();
		URL[] urls = Java9Utils.getUrls(cl);
		String[] paths = new String[urls.length];
		for (int i = 0; i < urls.length; i++) {
			try {
				paths[i] = new File(urls[i].toURI()).getAbsolutePath();
			} catch (URISyntaxException e) {
				throw new RuntimeException(e);
			}
		}
		jvmLaunchCmd.add(String.join(System.getProperty("path.separator"), paths));
		jvmLaunchCmd.add("aquacore.launcher.ui.LauncherWindow");

		File clientDir;
		try {
			clientDir = Config.getClientDir();
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
		clientDir.mkdirs();
		
		try {
			File update = new File(Config.getClientDir(), "update.bin");
			if(update.exists()) {
				update.deleteOnExit();
				File currentJar = Config.getJar();
				FileInputStream fis = new FileInputStream(update);
				FileOutputStream fos = new FileOutputStream(currentJar);
				byte[] buf = new byte[4096];
				int count = 0;
				while((count = fis.read(buf)) >= 0) {
					fos.write(buf, 0, count);
				}
				fis.close();
				fos.close();
			}
		} catch (URISyntaxException | IOException e1) {
			throw new RuntimeException(e1);
		}

		ProcessBuilder pb = new ProcessBuilder(jvmLaunchCmd);
		pb.directory(clientDir);
		pb.redirectOutput(new File(clientDir, "client.log"));
		pb.redirectErrorStream(true);
		try {
			/* Process p = */ pb.start();
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

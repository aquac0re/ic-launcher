package aquacore.launcher.launch;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.DigestInputStream;
import java.security.MessageDigest;

import javax.swing.SwingUtilities;

import org.apache.commons.codec.binary.Hex;

import aquacore.launcher.nocl.Java9Utils;
import aquacore.launcher.ui.Colors;
import aquacore.launcher.ui.ErrorPane;
import aquacore.launcher.ui.LauncherWindow;
import aquacore.launcher.ui.LoadingPane;
import aquacore.launcher.ui.LoginPane;
import aquacore.launcher.util.Config;
import aquacore.launcher.util.LauncherProperties;

public class ThreadLauncherVerifier extends Thread {
	{
		setName("Launcher verifier thread");
	}
	
	private void showErrorPane(final String text) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				ErrorPane.instance.setText(text);
				ErrorPane.instance.setTextColor(new Color(Colors.color_error));
				LauncherWindow.instance.setCurrentPane(ErrorPane.renderInstance);
			}
		});
	}

	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				LoadingPane.instance.setIndeterminate(true);
				LoadingPane.instance.setTitle("Верификация лаунчера...");
				LauncherWindow.instance.setCurrentPane(LoadingPane.renderInstance);
			}
		});
		
		try {
			if(md5succeed()) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						LauncherWindow.instance.setCurrentPane(LoginPane.renderInstance);
					}
				});
			} else {
				// даже не пытайтесь понять смысл этих строчек
				// Class.forName(LaunchMinecraft.class.getCanonicalName());
				Class.forName(LauncherProperties.class.getCanonicalName());
				Class.forName(Java9Utils.class.getCanonicalName());

				new ThreadLauncherUpdater().start();
				return;
			}
		} catch (Exception e) {
		    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		    try (PrintStream ps = new PrintStream(baos, true, "UTF-8")) {
		        e.printStackTrace(ps);
		    } catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}
		    
			showErrorPane("Произошла ошибка при верификации лаунчера:\n" + new String(baos.toByteArray(), StandardCharsets.UTF_8));
		}
	}

	/**
	 * Check whether the application has the true md5.
	 * 
	 * @return boolean - is the check was succeed.
	 */
	public static boolean md5succeed() throws Exception {
		if(!Config.RELEASE) {
			return true;
		}
		
		InputStream is = new URL(Config.launchServer + "/client/launcher.md5").openStream();
	
		final BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
		final MessageDigest md = MessageDigest.getInstance("MD5");
		final FileInputStream fis = new FileInputStream(Config.getJar());
		byte[] digest = null;
	
		try (final DigestInputStream dis = new DigestInputStream(fis, md)) {
	
			byte[] ba = new byte[4096];
			while (dis.read(ba) != -1) {
			}
	
			digest = md.digest();
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return Hex.encodeHexString(digest).toLowerCase().equals(rd.readLine().split("  ")[0]);
	}
}

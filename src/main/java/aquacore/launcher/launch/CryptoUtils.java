package aquacore.launcher.launch;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import aquacore.launcher.util.Config;

public class CryptoUtils {
	private static String getRandomHexString(int numchars) {
		Random r = new Random();
		StringBuffer sb = new StringBuffer();
		while (sb.length() < numchars) {
			sb.append(Integer.toHexString(r.nextInt()));
		}

		return sb.toString().substring(0, numchars);
	}

	private static byte[] getRandomByteArray(int numbytes) {
		Random r = new Random();
		byte[] arr = new byte[numbytes];
		r.nextBytes(arr);
		return arr;
	}

	public static String cryptoUID = getRandomHexString(256);
	public static byte[] cryptoKey = null;
	public static byte[] initVector = getRandomByteArray(16);

	private static final Random rand = new Random();

	public static BigInteger diffieHellmanGenb() {
		// System.out.println("[Launcher-Crypto] generating b...");
		return new BigInteger(512, rand);
	}

	public static BigInteger diffieHellmanGenB(BigInteger g, BigInteger p, BigInteger b) {
		// System.out.println("[Launcher-Crypto] generating B...");
		return g.modPow(b, p);
	}

	public static BigInteger diffieHellmanGenK(BigInteger A, BigInteger p, BigInteger b) {
		// System.out.println("[Launcher-Crypto] generating K...");
		return A.modPow(b, p);
	}

	public static byte[] bigIntegerToBytes(final BigInteger bigInteger) {
		byte[] bytes = bigInteger.toByteArray();
		if (bytes[0] == 0) {
			return Arrays.copyOfRange(bytes, 1, bytes.length);
		}
		return bytes;
	}

	public static byte[] generateAES256Key(BigInteger A, BigInteger p, BigInteger b) {
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.reset();
			byte[] K = bigIntegerToBytes(diffieHellmanGenK(A, p, b));
			digest.update(K);
			// System.out.println("[Launcher-Crypto] generating Key...");
			return digest.digest();
		} catch (NoSuchAlgorithmException e) {
			LaunchCrashReporter.crash(e);
			return null;
		}
	}

	public static byte[] encrypt(byte[] data, byte[] key, byte[] initVector) {
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, "AES"), new IvParameterSpec(initVector));
			return cipher.doFinal(data);
		} catch (Exception e) {
			LaunchCrashReporter.crash(e);
			return data;
		}
	}

	public static byte[] decrypt(byte[] data, byte[] key, byte[] initVector) {
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, "AES"), new IvParameterSpec(initVector));
			return cipher.doFinal(data);
		} catch (Exception e) {
			LaunchCrashReporter.crash(e);
			return data;
		}
	}

	public static void initCrypto() throws MalformedURLException, IOException, JSONException, DecoderException {
		InputStream is = null;
		JSONObject json = null;
		is = new URL(Config.launchServer + "/cryptokeys?uid=" + cryptoUID + "&iv=" + Hex.encodeHexString(initVector))
				.openStream();
		String str = IOUtils.toString(is, StandardCharsets.UTF_8);
		json = new JSONObject(str);
		BigInteger g = new BigInteger(Hex.decodeHex((("00" + json.getString("g")).toCharArray())));
		BigInteger p = new BigInteger(Hex.decodeHex((("00" + json.getString("p")).toCharArray())));
		BigInteger A = new BigInteger(Hex.decodeHex((("00" + json.getString("A")).toCharArray())));
		BigInteger b = CryptoUtils.diffieHellmanGenb();
		cryptoKey = CryptoUtils.generateAES256Key(A, p, b);
		BigInteger B = CryptoUtils.diffieHellmanGenB(g, p, b);

		InputStream shit = new URL(Config.launchServer + "/crypto2?uid=" + cryptoUID + "&B=" + B.toString(16))
				.openStream();
		shit.read();
		shit.close();

	}

	public static byte[] cryptoRequest(byte[] input, String target) throws IOException {
		HttpClient cl = HttpClientBuilder.create().build();
		HttpPost req = new HttpPost(Config.launchServer + "/crypto/" + target + "?uid=" + cryptoUID);
		ByteArrayEntity entity = new ByteArrayEntity(CryptoUtils.encrypt(input, cryptoKey, initVector));
		req.setHeader("content-type", "application/octet-stream");
		req.setEntity(entity);
		HttpResponse resp = cl.execute(req);
		return CryptoUtils.decrypt(EntityUtils.toByteArray(resp.getEntity()), cryptoKey, initVector);
	}
}

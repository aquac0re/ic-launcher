package aquacore.launcher.launch;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.SwingUtilities;

import aquacore.launcher.ui.LauncherWindow;
import aquacore.launcher.ui.LoadingPane;
import aquacore.launcher.util.Config;
import aquacore.launcher.util.DownloadQueue;
import aquacore.launcher.util.DownloadQueue.DownloadInfo;
import aquacore.launcher.util.DownloadQueue.ProgressHandler;

public class ThreadLauncherUpdater extends Thread {
	{
		setName("Launcher updater thread");
	}

	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				LoadingPane.instance.setIndeterminate(true);
				LoadingPane.instance.setTitle("Обновление лаунчера...");
				LauncherWindow.instance.setCurrentPane(LoadingPane.renderInstance);
			}
		});

		DownloadQueue q = new DownloadQueue();
		try {
			q.addDownload(new DownloadInfo(new URL(Config.launchServer + "/client/launcher.jar"),
					new File(Config.getClientDir(), "update.bin")));
			q.setProgressHandler(new ProgressHandler() {

				@Override
				public void updateProgress(double percentage, final String state) {
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							LoadingPane.instance.setIndeterminate(false);
							LoadingPane.instance.setTitle("Обновление лаунчера: " + state + "...");
						}
					});
				}

				@Override
				public void onStart() {
				}

				@Override
				public void onError(Throwable e) {
					LaunchCrashReporter.crash(e);
				}

				@Override
				public void onDone() {
					LauncherRelauncher.relaunch();
				}

				@Override
				public void onCancel() {
				}
			});
			q.startDownloading();
		} catch (MalformedURLException | URISyntaxException e) {
			LaunchCrashReporter.crash(e);
		}
	}
}

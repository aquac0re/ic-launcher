package aquacore.launcher.launch;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

import aquacore.launcher.DontObfuscate;
import aquacore.launcher.data.LoginSelector.AuthResult;
import aquacore.launcher.ui.LauncherWindow;
import aquacore.launcher.update.ClientSpec;
import net.minecraft.launchwrapper.Launch;

@DontObfuscate
public enum ClientLaunchMode {
	@SerializedName("normal")
	NORMAL {
		@Override
		public void launch(String name, ClientSpec spec, AuthResult auth, Launch launch) {
			List<String> arguments = new ArrayList<>();
			File clientDir;
			try {
				clientDir = ClientSpec.getDirectory(name);
			} catch (URISyntaxException e) {
				LaunchCrashReporter.crash(e);
				return;
			}
			for (String argToSubst : spec.launchArguments) {
				arguments.add(argToSubst.replace("%username%", auth.username)
						.replace("%token%", auth.accessToken)
						.replace("%uuid%", auth.uuid)
						.replace("%client_dir%", clientDir.getAbsolutePath()));
			}
			launch.launch(arguments.toArray(new String[arguments.size()]));
		}
	},
	@SerializedName("applet")
	APPLET {
		@Override
		public void launch(String name, ClientSpec spec, AuthResult auth, Launch launch) {
			List<String> arguments = new ArrayList<>();
			File clientDir;
			try {
				clientDir = ClientSpec.getDirectory(name);
			} catch (URISyntaxException e) {
				LaunchCrashReporter.crash(e);
				return;
			}
			for (String argToSubst : spec.launchArguments) {
				arguments.add(argToSubst.replace("%username%", auth.username)
						.replace("%token%", auth.accessToken)
						.replace("%uuid%", auth.uuid)
						.replace("%client_dir%", clientDir.getAbsolutePath()));
			}
			List<String> appletArguments = new ArrayList<>();
			for (String argToSubst : spec.appletArguments) {
				appletArguments.add(argToSubst.replace("%username%", auth.username)
						.replace("%token%", auth.accessToken)
						.replace("%uuid%", auth.uuid)
						.replace("%client_dir%", clientDir.getAbsolutePath()));
			}
			LauncherWindow.instance.appletWrapper.runApplet(launch
					.getApplet(arguments.toArray(new String[arguments.size()])), spec.appletShutdown, appletArguments
							.toArray(new String[appletArguments.size()]));
		}
	};

	public abstract void launch(String name, ClientSpec spec, AuthResult auth, Launch launch);
}

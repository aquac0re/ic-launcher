package aquacore.launcher.launch;

import java.io.IOException;

import javax.swing.SwingUtilities;

import org.apache.commons.codec.DecoderException;
import org.json.JSONException;

import aquacore.launcher.ui.LauncherWindow;
import aquacore.launcher.ui.LoadingPane;

public class ThreadCryptoSetup extends Thread {
	{
		setName("Launcher crypto setup thread");
	}
	
	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				LoadingPane.instance.setIndeterminate(true);
				LoadingPane.instance.setTitle("Инициализация...");
				LauncherWindow.instance.setCurrentPane(LoadingPane.renderInstance);
			}
		});
		
		try {
			CryptoUtils.initCrypto();
			new ThreadLauncherVerifier().start();;
		} catch (JSONException | IOException | DecoderException e) {
			LaunchCrashReporter.crash(e);
		}
	}
	
}

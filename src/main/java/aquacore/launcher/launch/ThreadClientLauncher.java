package aquacore.launcher.launch;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.SwingUtilities;

import aquacore.launcher.data.LoginSelector.AuthResult;
import aquacore.launcher.ui.LauncherWindow;
import aquacore.launcher.ui.LoadingPane;
import aquacore.launcher.update.ClientChecker;
import aquacore.launcher.update.ClientSpec;
import aquacore.launcher.util.DownloadQueue.ProgressHandler;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ThreadClientLauncher extends Thread {
	{
		setName("Launcher client check/launch thread");
	}

	private final String name;
	private final ClientSpec spec;
	private final AuthResult auth;

	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				LoadingPane.instance.setIndeterminate(true);
				LoadingPane.instance.setTitle("Проверка клиента...");
				LauncherWindow.instance.setCurrentPane(LoadingPane.renderInstance);
			}
		});
		ClientChecker checker = new ClientChecker(name, spec);
		try {
			checker.doCheck();
		} catch (URISyntaxException | IOException e1) {
			LaunchCrashReporter.crash(e1);
			return;
		}
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				LoadingPane.instance.setIndeterminate(true);
				LoadingPane.instance.setTitle("Удаление файлов...");
				LauncherWindow.instance.setCurrentPane(LoadingPane.renderInstance);
			}
		});
		checker.doRemoval();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				LoadingPane.instance.setIndeterminate(true);
				LoadingPane.instance.setTitle("Загрузка клиента...");
				LauncherWindow.instance.setCurrentPane(LoadingPane.renderInstance);
			}
		});
		checker.getDownloadQueue().setProgressHandler(new ProgressHandler() {

			@Override
			public void updateProgress(double percentage, final String state) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						LoadingPane.instance.setIndeterminate(false);
						LoadingPane.instance.setMinimum(0);
						LoadingPane.instance.setMaximum(100);
						LoadingPane.instance.setValue((int) percentage);
						LoadingPane.instance.setTitle("Загрузка клиента: " + (int) percentage + "% " + state + "...");
						LauncherWindow.instance.setCurrentPane(LoadingPane.renderInstance);
					}
				});
			}

			@Override
			public void onStart() {
			}

			@Override
			public void onError(Throwable e) {
				LaunchCrashReporter.crash(e);
			}

			@Override
			public void onDone() {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						LoadingPane.instance.setIndeterminate(true);
						LoadingPane.instance.setTitle("Запуск клиента...");
						LauncherWindow.instance.setCurrentPane(LoadingPane.renderInstance);
					}
				});

				if (ClientLauncher.doCheckPreconditions()) {
					ClientLauncher.launch(name, spec, auth);
				}

			}

			@Override
			public void onCancel() {
			}
		});
		checker.getDownloadQueue().startDownloading();
	}
}

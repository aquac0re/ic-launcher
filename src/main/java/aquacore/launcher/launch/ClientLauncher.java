package aquacore.launcher.launch;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import aquacore.launcher.data.LoginSelector.AuthResult;
import aquacore.launcher.platform.PlatformUtils;
import aquacore.launcher.update.ClientSpec;
import aquacore.launcher.util.ClassPathHacker;
import aquacore.launcher.util.Config;
import net.minecraft.launchwrapper.Launch;

public class ClientLauncher {
	public static boolean doCheckPreconditions() {
		if (Config.RELEASE) {
			RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
			List<String> arguments = runtimeMxBean.getInputArguments();
			boolean javaargcheck = false;
			for (String argument : arguments) {
				if (argument.equals("-XX:+DisableAttachMechanism")) {
					javaargcheck = true;
				}
				if (argument.equals("-XX:-DisableAttachMechanism")) {
					javaargcheck = false;
					break;
				}
			}

			if (!javaargcheck) {
				LaunchCrashReporter.crash(new SecurityException("Целостность лаунчера нарушена: включено Attach API"));
				return false;
			}

			try {
				if (!ThreadLauncherVerifier.md5succeed()) {
					LaunchCrashReporter
							.crash(new SecurityException("Целостность лаунчера нарушена: лаунчер модифицирован"));
					return false;
				}
			} catch (Exception e1) {
				LaunchCrashReporter
						.crash(new SecurityException("Целостность лаунчера нарушена: лаунчер не прошёл проверку MD5"));
				return false;
			}
		}
		return true;

	}

	public static void launch(String name, ClientSpec spec, AuthResult auth) {
		try {
			File clientDir = ClientSpec.getDirectory(name);
			File libDir = new File(clientDir, spec.classpathDir);
			File nativesDir = new File(clientDir, spec.nativesDir);

			System.out.println("Starting Minecraft");
			System.out.flush();

			System.setProperty("fml.ignoreInvalidMinecraftCertificates", "true");
			System.setProperty("fml.ignorePatchDiscrepancies", "true");
			System.setProperty("minecraft.applet.TargetDirectory", clientDir.getAbsolutePath());
			System.setProperty("minecraft.applet.WrapperClass", "aquacore.launcher.applet.AppletWrapper");

			File[] fileList = libDir.listFiles();
			Arrays.sort(fileList, new Comparator<File>() {
				@Override
				public int compare(File obj1, File obj2) {
					if (obj1 == null) {
						return -1;
					}
					if (obj2 == null) {
						return 1;
					}
					if (obj1.equals(obj2)) {
						return 0;
					}
					return obj1.compareTo(obj2);
				}
			});

			for (File f : fileList) {
				if (!f.isDirectory()) {
					if (f.getName().endsWith(".jar")) {
						System.out.println("Trying to load " + f.getAbsolutePath());
						ClassPathHacker.addFile(f);
					}
				}
			}
			
			ClassPathHacker.setLibraryPath(nativesDir.getAbsolutePath());
			
			PlatformUtils.get().setCWD(clientDir);
			
			Launch launch = new Launch();
			
			spec.launchMode.launch(name, spec, auth, launch);
		} catch (Exception e) {
			LaunchCrashReporter.crash(e);
		}
	}
}

package aquacore.launcher.util;

import java.lang.reflect.Method;

public class BypassExit {
	public static void exit(int code) {
		try {
			Class<?> shutdown = Class.forName("java.lang.Shutdown");
			Method method = shutdown.getDeclaredMethod("exit", int.class);
			method.setAccessible(true);
			method.invoke(null, code);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(code);
		}
	}
}

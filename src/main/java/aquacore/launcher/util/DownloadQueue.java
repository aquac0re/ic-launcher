package aquacore.launcher.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class DownloadQueue implements Runnable {
	/*@Deprecated
	public static final DownloadQueue instance = new DownloadQueue();*/

	public static interface ProgressHandler {
		public void updateProgress(double percentage, String state);

		public void onDone();

		public void onCancel();

		public void onStart();
		
		public void onError(Throwable e);
	}

	public static class DownloadInfo {
		public DownloadInfo() {
			super();
		}

		public DownloadInfo(URL url, File path) {
			super();
			this.url = url;
			this.path = path;
		}

		public URL url;
		public File path;
	}

	private ProgressHandler phandler;
	private volatile Queue<DownloadInfo> queue = new LinkedBlockingQueue<>();
	private Queue<File> removeQueue = new LinkedBlockingQueue<>();
	private Thread downloadThread;

	private int size;
	private volatile boolean isDone = false;

	public DownloadQueue() {
	}

	public void setProgressHandler(ProgressHandler handler) {
		this.phandler = handler;
	}

	public void startDownloading() {
		isDone = false;
		if (phandler != null) {
			phandler.onStart();
		}
		size = queue.size();
		File removeFile = null;
		while ((removeFile = removeQueue.poll()) != null) {
			removeFile.delete();
		}

		downloadThread = new Thread(this);
		downloadThread.setName("DLThread");
		downloadThread.start();
	}

	public void cancel() {
		downloadThread.interrupt();
		phandler.onCancel();

		isDone = false;
	}

	public void resetQueue() {
		this.queue.clear();
		this.removeQueue.clear();
	}

	public void addDownload(DownloadInfo info) {
		this.queue.add(info);
	}

	public void addRemove(File file) {
		this.removeQueue.add(file);
	}

	@Override
	public void run() {
		DownloadInfo info;

		byte[] buf = new byte[1024 * 512];
		synchronized (queue) {
			while ((info = queue.poll()) != null) {
				try {
					if (Thread.currentThread().isInterrupted())
						return;
					System.out.println("Downloading " + info.url.toString());
					if (this.phandler != null)
						this.phandler.updateProgress((1.0 - (queue.size() + 1) / (double) size) * 100.0, "(? Kb/s) ");
					URLConnection uc = info.url.openConnection();
					uc.connect();
					int cl = uc.getContentLength();
					if(cl == 0 || cl == -1) {
						cl = 1;
					}
					InputStream is = uc.getInputStream();

					info.path.getParentFile().mkdirs();

					// FileUtils.copyInputStreamToFile(is, info.path);
					FileOutputStream fos = new FileOutputStream(info.path, false);
					BufferedOutputStream bos = new BufferedOutputStream(fos);
					int count;
					long start = System.nanoTime();
					long totalRead = 0;

					final double NANOS_PER_SECOND = 1000000000.0;
					final double BYTES_PER_KIB = 1024;
					
					while ((count = is.read(buf)) != -1) {

						if (Thread.currentThread().isInterrupted())
							return;
						bos.write(buf, 0, count);
						totalRead += count;
						double speed = NANOS_PER_SECOND / BYTES_PER_KIB * totalRead / (System.nanoTime() - start + 1);
						if (this.phandler != null)
							this.phandler
									.updateProgress((1.0 - (queue.size() + 1) / (double) size + (1.0 / (double) size) * ((double) totalRead / cl)) * 100.0,
											"(" + (int) speed + " Kb/s)");
					}
					bos.close();
					fos.close();
					is.close();
					System.out.println("Downloaded " + info.url.toString());
				} catch (IOException e) {
					System.out.println("Download failed.");
					e.printStackTrace();
					if (this.phandler != null) {
						this.phandler.onError(e);
					}
					cancel();
					return;
				}

			}
		}

		if (!isDone) {
			isDone = true;
			if (this.phandler != null) {
				this.phandler.onDone();
			}

		}
	}
}

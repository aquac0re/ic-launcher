package aquacore.launcher.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;

import aquacore.launcher.launch.LaunchCrashReporter;

public class LauncherProperties extends Properties {
	private static final long serialVersionUID = 2083747059019063304L;
	public static final LauncherProperties instance = new LauncherProperties();

	public LauncherProperties() {
		super();
		load();
	}

	public void load() {
		try {
			FileInputStream fis = new FileInputStream(new File(Config.getClientDir(), "launcher.cfg"));
			load(fis);
			fis.close();
		} catch (FileNotFoundException e) {
			System.out.println("launcher.cfg does not exist, creating one...");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			LaunchCrashReporter.crash(e);
		}
		if (this.getProperty("username") == null) {
			this.setProperty("username", "");
		}
		if (this.getProperty("password") == null) {
			this.setProperty("password", "");
		}
		if (this.getProperty("xmx") == null) {
			this.setProperty("xmx", (OsUtils.isWindows() && !OsUtils.is64()) ? "1024" : "2048");
		}
		if (this.getProperty("client") == null) {
			this.setProperty("client", "");
		}
		if (this.getProperty("titleColor") == null) {
			this.setProperty("titleColor", String.valueOf(0x009688));
		}
		if (this.getProperty("fsBounds") == null) {
			this.setProperty("fsBounds", "0");
		}
		store();
	}

	public void store() {
		try {
			FileOutputStream fos = new FileOutputStream(new File(Config.getClientDir(), "launcher.cfg"), false);
			store(fos, "Launcher config");
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			LaunchCrashReporter.crash(e);
		}

	}
}

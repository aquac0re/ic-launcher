package aquacore.launcher.util;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class Utils {

	/*public static class ClientBox {
		public String id = "";
		public String name = "";
		public String pingHost = "";
		public int pingPort = 25565;
		public String status = "?/?";
		public boolean online = false;

		public void updateStatus() {
			status = pingServer(pingHost, pingPort);
			if (status.equals("?/?"))
				online = false;
			else
				online = true;
		}
	}

	public static List<ClientBox> clientList = null;

	/*public static ClientBox lookupClient(String id) {
		List<ClientBox> cl = receiveClientList();
		if (cl == null)
			return null;
		for (ClientBox cb : cl) {
			if (cb.id.equals(id))
				return cb;
		}
		return null;
	}*/

	/*public static List<ClientBox> receiveClientList() {
		if (clientList != null)
			return clientList;
		InputStream is = null;
		JSONArray json = null;
		ArrayList<ClientBox> res = new ArrayList<>();
		try {
			is = new URL(Config.launchServer + "/client/clients.json").openStream();
			String str = IOUtils.toString(is, StandardCharsets.UTF_8);
			json = new JSONArray(str);
			for (Object obj : json) {
				if (!(obj instanceof JSONObject)) {
					return null;
				}
				JSONObject jsonObj = (JSONObject) obj;
				ClientBox cb = new ClientBox();
				cb.id = jsonObj.getString("id");
				cb.name = jsonObj.getString("name");
				cb.pingHost = jsonObj.getString("pingAddress");
				cb.pingPort = jsonObj.getInt("pingPort");
				cb.updateStatus();
				res.add(cb);
			}
		} catch (JSONException | IOException e) {
			e.printStackTrace();
			if (is != null)
				try {
					is.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			return null;
		}

		return clientList = res;
	}*/

	public static String register(String username, String password) {
		HttpClient cl = HttpClientBuilder.create().build();
		HttpPost req = new HttpPost(Config.launchServer + "/register");
		JSONObject iobj = new JSONObject();
		iobj.put("username", username);
		iobj.put("password", password);

		try {
			StringEntity entity = new StringEntity(iobj.toString());
			req.setHeader("content-type", "application/json");
			req.setEntity(entity);
			HttpResponse resp = cl.execute(req);
			String sresp = EntityUtils.toString(resp.getEntity());
			JSONObject oobj = new JSONObject(sresp);
			if (oobj.has("error") && !oobj.isNull("error")) {
				return oobj.getString("error");
			} else {
				return null;
			}
		} catch (IOException e) {
			return "Неизвестная ошибка";
		}

	}

	public static String pingServer(String ip, int port) {
		try {
			Socket socket = new Socket(ip, port);
			try {
				socket.setSoTimeout(10000);
			} catch (SocketException e) {
			}

			socket.getOutputStream().write(new byte[] { -2, 1 });
			byte[] response = new byte[4096];
			int size = socket.getInputStream().read(response);
			socket.close();
			ByteBuffer buf = ByteBuffer.wrap(response);

			if (size < 3) {
				return "?/?";
			}

			if (buf.get() != -1) {
				return "?/?";
			}

			if (buf.getShort() < 0) {
				return "?/?";
			}
			String s = new String(Arrays.copyOfRange(response, 3, response.length), Charset.forName("UTF-16BE"));
			String[] ss = s.split("\\x00");
			if (ss.length < 6)
				return "?/?";

			return ss[4] + "/" + ss[5];
		} catch (IOException e) {
			e.printStackTrace();
			return "?/?";
		}
	}

	public static File getJavaExecutable() {
		String javaHome = System.getProperty("java.home");
		File f = new File(javaHome);
		f = new File(f, "bin");
		f = new File(f, OsUtils.isWindows() ? "javaw.exe" : "java");
		return f;
	}

}

package aquacore.launcher.util;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

public class Config {
	public static final String launchServer = "http://207.180.208.251:25801";
	public static final String pingServerIP = "207.180.208.251";
	public static final int pingServerPort = 25800;
	public static final String clientDir = "aquac0re";
	public static final boolean RELEASE = true;

	public static File getClientDir() throws URISyntaxException {
		URL location = Config.class.getProtectionDomain().getCodeSource().getLocation();
		File f = new File(new File(location.toURI()).getParentFile().getAbsoluteFile(), Config.clientDir);
		f.mkdirs();
		/*if (!noprops) {
			return new File(f, LauncherProperties.instance.getProperty("client"));
		}*/
		return f;
	}

	public static File getJar() throws URISyntaxException {
		URL location = Config.class.getProtectionDomain().getCodeSource().getLocation();
		File f = new File(location.toURI());
		return f;
	}

	public static URL getJarURL() throws URISyntaxException {
		URL location = Config.class.getProtectionDomain().getCodeSource().getLocation();
		return location;
	}
}
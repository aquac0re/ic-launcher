package aquacore.launcher;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import org.apache.commons.codec.binary.Base64;

import aquacore.launcher.AuthHelper.AuthResult;
import aquacore.launcher.AuthHelper.ClientBox;
import aquacore.launcher.DownloadQueue.ProgressHandler;
import aquacore.launcher.gui.AquaCoreFlatButton;
import aquacore.launcher.gui.AquaCoreImageButton;
import aquacore.launcher.gui.AquaCoreLCR;
import aquacore.launcher.gui.AquaCoreLabel;
import aquacore.launcher.gui.AquaCorePassField;
import aquacore.launcher.gui.AquaCoreTextField;
import aquacore.launcher.gui.BgPane;
import aquacore.launcher.gui.NumberSetter;
import aquacore.launcher.util.ComponentMover;
import aquacore.launcher.util.ComponentResizer;

public class LauncherWindow extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;
	private JLabel background;
	private JButton minimizeButton;
	private JButton closeButton;

	private JTabbedPane pane;
	private AquaCoreFlatButton loginButton;
	private AquaCoreFlatButton registerButton;

	private JPanel titlebar;
	private JPanel loginState;

	private AquaCoreLabel loginLabel;
	private AquaCoreTextField loginField;
	private AquaCoreLabel passwordLabel;
	private AquaCorePassField passwordField;

	private AquaCoreFlatButton l_loginButton;
	private AquaCoreLabel l_error;

	private JPanel regState;
	private AquaCoreLabel r_confirmPassword;
	private AquaCorePassField r_confirmPasswordField;
	private AquaCoreLabel r_error;
	private AquaCoreFlatButton r_registerButton;

	private JLabel loadingLabel;
	private JProgressBar loadingBar;

	private Canvas skinView = new Canvas();
	private AquaCoreFlatButton backButton;

	private AquaCoreLabel memoryLabel;
	private AquaCoreLabel memoryStateLabel;
	private NumberSetter setter;

	boolean isLogin = true;

	private Thread monitorUpdateThread = new Thread(new Runnable() {

		@Override
		public void run() {

			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				return;
			}
			final ClientBox client = AuthHelper.lookupClient(LauncherProperties.instance.getProperty("client"));
			while (true) {

				client.updateStatus();
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						if (l_loginButton != null) {
							l_loginButton.setText("Войти (" + client.status + ")");
						}
					}
				});

				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					return;
				}
			}
		}
	});


	private static final int LOGIN_BUTTON_LOGIN_OFFSET_X = 0;
	private static final int LOGIN_BUTTON_REGISTER_OFFSET_X = 206;
	private static final int LOGIN_PANE_OFFSET_Y = 6;
	private static final int LOGIN_SNIP_WIDTH = 400;
	private static final int LOGIN_SNIP_HEIGHT = 513;

	private static final int STATE_OFF_X = 100;

	private static final int LOADING_WIDTH = 400;
	private static final int LOADING_HEIGHT = 100;

	private static final int SKINS_CANVAS_INSETS = 100;
	private static final int SKINS_BUTTON_OFFX = 0;

	private static final int SETTINGS_WIDTH = 400;
	private static final int SETTINGS_HEIGHT = 500;

	private static final int STATE_LOADING = 0;
	private static final int STATE_LOGIN = 2;
	private static final int STATE_SETTINGS = 1;
	private static final int STATE_SKINS = 3;

	private int state = LauncherWindow.STATE_LOGIN;

	private BgPane loginPanel;
	private BgPane loadingPanel;
	private BgPane skinsPanel;
	private BgPane settingsPanel;
	public Canvas gamePanel = new Canvas();

	private Font appFont;

	private AquaCoreFlatButton changeSkinButton;

	public static LauncherWindow instance;

	private ActionListener changeSkinListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			setStateSkins();
		}
	};

	private ActionListener backListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			setter.setCurrentNumber(Integer.parseInt(LauncherProperties.instance.getProperty("xmx")));
			setStateLogin();
		}
	};

	private AquaCoreFlatButton l_settingsButton;

	private AquaCoreFlatButton st_saveButton;

	private AquaCoreLabel l_selectClient;

	private JComboBox<AuthHelper.ClientBox> l_scCbox;

	public static void main(String[] args) {
		instance = new LauncherWindow();
	}

	/**
	 * Create the frame.
	 */
	public LauncherWindow() {
		UIManager.getLookAndFeelDefaults().put("TabbedPane.tabInsets", new Insets(10, 10, 10, 10));

		setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/server3_copy.png")));
		setTitle("AquaC0RE Launcher");

		try {
			appFont = Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("/Comfortaa-Regular.ttf"));
		} catch (FontFormatException | IOException e1) {
			appFont = new Font("Arial", Font.PLAIN, 20);
			e1.printStackTrace();
		}

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setSize(1280, 720);
		setSize();
		setupUI();

		setUndecorated(true);
		final ComponentResizer r = new ComponentResizer();

		titlebar = new JPanel();
		titlebar.setLocation(0, 0);
		contentPane.add(titlebar);
		titlebar.setLayout(null);
		titlebar.setSize(getWidth(), 30);

		background = new JLabel() {

			private static final long serialVersionUID = 1L;

			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);

				g.setColor(new Color(LauncherWindow.color_titlebar));
				g.fillRect(0, 0, getWidth(), getHeight());

				g.setFont(new Font("Arial", Font.BOLD, 15));
				g.setColor(new Color(LauncherWindow.color_bg));

				((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				g.setFont(appFont.deriveFont(15f));
				g.drawString("AquaC0RE Launcher", 10, 20);
			}

		};

		background.setBounds(0, 0, getWidth() - 80, 30);
		titlebar.add(background);
		background.setMinimumSize(new Dimension(Toolkit.getDefaultToolkit().getScreenSize().width, 30));

		new ComponentMover(JFrame.class, background);

		minimizeButton = new AquaCoreImageButton("/minimizeButton.png", 0x1769ED, 0x1250B5);
		minimizeButton.setBounds(getWidth() - 80, 0, 40, 30);
		titlebar.add(minimizeButton);
		minimizeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setState(ICONIFIED);
			}
		});

		closeButton = new AquaCoreImageButton("/closeButton.png", LauncherWindow.color_error,
				LauncherWindow.color_error_d);
		closeButton.setBounds(getWidth() - 40, 0, 40, 30);
		titlebar.add(closeButton);
		closeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				onClose();
			}
		});
		r.registerComponent(this);
		r.setSnapSize(new Dimension(3, 3));
		setVisible(true);

		addComponentListener(new ComponentAdapter() {

			@Override
			public void componentResized(ComponentEvent e) {
				updateTitleSize();
				updateContentSize();
				repaint();
			}

		});

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				onClose();
			}
		});

		updateContentSize();

		setLoginCallback(new ActionListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void actionPerformed(ActionEvent e) {
				LauncherProperties.instance.setProperty("username", getLoginField().getText());
				LauncherProperties.instance.setProperty("password",
						Base64.encodeBase64String(getPasswordField().getText().getBytes()));
				LauncherProperties.instance.store();
				AuthResult res = AuthHelper.authorize(getLoginField().getText(), getPasswordField().getText());
				if (res.error != null) {
					l_error.setText(res.error);
					l_error.setVisible(true);
				} else {
					l_error.setVisible(false);
					setStateLoading();
					loadingBar.setIndeterminate(true);
					loadingLabel.setText("Проверка клиента...");
					new Thread(new ClientChecker()).start();
				}
			}
		});

		setRegistrationCallback(new ActionListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!getPasswordField().getText().equals(getConfirmationPasswordField().getText())) {
					r_error.setForeground(new Color(color_error));
					r_error.setText("Пароли не совпадают");
					r_error.setVisible(true);
				} else {
					String res = AuthHelper.register(getLoginField().getText(), getPasswordField().getText());
					if (res != null) {
						r_error.setForeground(new Color(color_error));
						r_error.setText(res);
						r_error.setVisible(true);
					} else {
						r_error.setForeground(new Color(color_success));
						r_error.setText("Вы успешно зарегистрировались");
						r_error.setVisible(true);
					}
				}
			}
		});

		DownloadQueue.instance.setProgressHandler(new ProgressHandler() {

			@Override
			public void updateProgress(double percentage, String state) {
				loadingLabel.setText(((int) percentage) + "%: " + state);
				loadingBar.setIndeterminate(false);
				loadingBar.setValue((int) percentage);
			}

			@SuppressWarnings("deprecation")
			@Override
			public void onDone() {
				loadingBar.setIndeterminate(true);
				loadingLabel.setText("Запуск клиента...");
				// LaunchMinecraft.launch(getLoginStateLoginField().getText(),
				// getLoginStatePasswordField().getText());
				// setStateGame();
				monitorUpdateThread.interrupt();
				LaunchMinecraft.launch(getLoginField().getText(), getPasswordField().getText());
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStart() {
				loadingBar.setIndeterminate(false);
			}
		});

		gamePanel.setSize(256, 256);
	}

	private void setSize() {
		boolean c = false;
		File file = null;
		try {
			file = new File(Config.getClientDir(true), "screenData.txt");
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
			return;
		}

		c = file.exists();

		if (c) {
			try (BufferedReader r = new BufferedReader(new FileReader(file))) {
				String[] line = r.readLine().split(" ");

				setSize(Integer.parseInt(line[0].trim()), Integer.parseInt(line[1].trim()));
				setLocation(Integer.parseInt(line[2].trim()), Integer.parseInt(line[3].trim()));

				r.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			setMinimumSize(new Dimension(430, 560));
		} else {
			setSize(430, 560);
			setMinimumSize(getSize());
			setLocationRelativeTo(null);
		}
	}

	private void onClose() {
		try {
			File file = new File(Config.getClientDir(true), "screenData.txt");
			if (!file.exists()) {
				file.createNewFile();
			}

			try (BufferedWriter w = new BufferedWriter(new FileWriter(file))) {
				w.write(getWidth() + " " + getHeight() + " " + getX() + " " + getY());
				w.close();
			}
		} catch (IOException | URISyntaxException e1) {
			e1.printStackTrace();
		}

		BypassExit.exit(0);
		System.exit(0);
	}

	public JTextField getLoginField() {
		return loginField;
	}

	public JPasswordField getPasswordField() {
		return passwordField;
	}

	public JPasswordField getConfirmationPasswordField() {
		return r_confirmPasswordField;
	}

	/**
	 * Set the callback for clicking the `login` button.
	 * 
	 * @param l
	 *            - the listener for button.
	 */
	public void setLoginCallback(ActionListener l) {
		l_loginButton.addActionListener(l);
	}

	/**
	 * Set the callback for clicking the `register` button.
	 * 
	 * @param l
	 *            - the listener for button.
	 */
	public void setRegistrationCallback(ActionListener l) {
		r_registerButton.addActionListener(l);
	}

	/**
	 * Sets the current state to Login state
	 * 
	 * @author yakute
	 **/
	public void setStateLogin() {
		state = LauncherWindow.STATE_LOGIN;
		contentPane.remove(gamePanel);
		contentPane.remove(loadingPanel);
		contentPane.remove(skinsPanel);
		contentPane.remove(settingsPanel);
		repaint();
		contentPane.add(loginPanel);

		updateContentSize();
		setVisible(true);
	}

	/**
	 * Sets the current state to Game state
	 * 
	 **/
	public void setStateGame() {
		contentPane.remove(loadingPanel);
		contentPane.remove(loginPanel);
		contentPane.remove(skinsPanel);
		contentPane.remove(settingsPanel);
		repaint();
		contentPane.add(gamePanel);

		updateContentSize();
		setVisible(true);
	}

	/**
	 * Sets the current state to loading state
	 * 
	 **/
	public void setStateLoading() {
		state = LauncherWindow.STATE_LOADING;
		contentPane.remove(gamePanel);
		contentPane.remove(loginPanel);
		contentPane.remove(skinsPanel);
		contentPane.remove(settingsPanel);
		repaint();
		contentPane.add(loadingPanel);

		updateContentSize();
		setVisible(true);
	}

	/**
	 * Sets the current state to skins state
	 * 
	 * @author yakute
	 **/
	public void setStateSkins() {
		JOptionPane.showMessageDialog(this, "Скины ещё не доступны.", "Внимание!", JOptionPane.WARNING_MESSAGE);
		return;
		/*
		 * state = LauncherWindow.STATE_SKINS; contentPane.remove(gamePanel);
		 * contentPane.remove(loadingPanel); contentPane.remove(loginPanel);
		 * contentPane.remove(settingsPanel); repaint();
		 * 
		 * contentPane.add(skinsPanel); skinsPanel.add(backButton);
		 * 
		 * updateContentSize(); setVisible(true);
		 */
	}

	public void setStateSettings() {
		state = STATE_SETTINGS;
		contentPane.remove(gamePanel);
		contentPane.remove(loadingPanel);
		contentPane.remove(loginPanel);
		contentPane.remove(skinsPanel);
		repaint();
		contentPane.add(settingsPanel);
		settingsPanel.add(backButton);

		updateContentSize();
		setVisible(true);
	}

	// ************************INIT*****************************//
	// ************************>><<*****************************//
	// ************************>><<*****************************//
	// ************************>><<*****************************//

	private void setupUI() {
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0x26a7a7));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		skinsPanel = createSkinsPane();
		loginPanel = createLoginPane();
		loginPanel.setBounds(0, 30, getWidth(), getHeight() - 30);
		loginPanel.setBorder(new EmptyBorder(0, 0, 0, 0));

		loadingPanel = createLoadingPane();
		loadingPanel.setBounds(0, 30, getWidth(), getHeight() - 30);
		loadingPanel.setBorder(new EmptyBorder(0, 0, 0, 0));

		skinsPanel.setBounds(0, 30, getWidth(), getHeight() - 30);
		skinsPanel.setBorder(new EmptyBorder(0, 0, 0, 0));

		settingsPanel = createSettingsPanel();
		settingsPanel.setBounds(0, 30, getWidth(), getHeight() - 30);
		settingsPanel.setBorder(new EmptyBorder(0, 0, 0, 0));

		switch (state) {
		case LauncherWindow.STATE_LOGIN:
			contentPane.add(loginPanel);
			break;
		case LauncherWindow.STATE_LOADING:
			contentPane.add(loadingPanel);
			break;
		case LauncherWindow.STATE_SKINS:
			contentPane.add(skinsPanel);
			break;
		case LauncherWindow.STATE_SETTINGS:
			contentPane.add(settingsPanel);
			break;
		}
	}

	/**
	 * Creating the Login panel
	 * 
	 * @return JPanel - the login panel.
	 * @author yakute
	 **/
	private BgPane createSettingsPanel() {
		final BgPane result = new BgPane() {
			private static final long serialVersionUID = 1L;

			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);

				g.setColor(new Color(LauncherWindow.color_bg));
				g.fillRect(getWidth() / 2 - SETTINGS_WIDTH / 2, getHeight() / 2 - SETTINGS_HEIGHT / 2, SETTINGS_WIDTH,
						SETTINGS_HEIGHT);
			}
		};

		memoryLabel = new AquaCoreLabel("Выделение памяти: ");
		memoryLabel.setForeground(new Color(LauncherWindow.color_accent));
		memoryLabel.setBounds(getWidth() / 2 - SETTINGS_WIDTH / 2 + 50, getHeight() / 2 - SETTINGS_HEIGHT / 2 + 30, 300,
				50);
		result.add(memoryLabel);

		memoryStateLabel = new AquaCoreLabel("");
		memoryStateLabel.setForeground(new Color(LauncherWindow.color_accent));
		memoryStateLabel.setBounds(getWidth() / 2 - SETTINGS_WIDTH / 2 + 50,
				getHeight() / 2 - SETTINGS_HEIGHT / 2 + 100, 300, 50);
		result.add(memoryStateLabel);

		setter = new NumberSetter(512, (OsUtils.isWindows() && !OsUtils.is64()) ? 1024 : 8192, memoryStateLabel);
		setter.setCurrentNumber(Integer.parseInt(LauncherProperties.instance.getProperty("xmx")));
		setter.setBounds(getWidth() / 2 - SETTINGS_WIDTH / 2 + 50, getHeight() / 2 - SETTINGS_HEIGHT / 2 + 80, 300, 20);
		result.add(setter);

		result.add(backButton);

		st_saveButton = new AquaCoreFlatButton("Сохранить", 1);
		st_saveButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				LauncherProperties.instance.setProperty("xmx", String.valueOf(setter.getNumber()));
				LauncherProperties.instance.store();
				Main.main(new String[] {});
			}
		});
		result.add(st_saveButton);

		// TODO: settings setup

		return result;
	}

	/**
	 * Creating the Login panel
	 * 
	 * @return JPanel - the login panel.
	 * @author yakute
	 **/
	private BgPane createLoginPane() {
		// TODO: monitoring
		BgPane result = new BgPane();

		loginButton = new AquaCoreFlatButton("Вход");
		loginButton.setSize(199, 30);
		loginButton.setMinimumSize(new Dimension(180, 30));

		registerButton = new AquaCoreFlatButton("Регистрация");
		registerButton.setSize(199, 30);
		registerButton.setMinimumSize(new Dimension(180, 30));

		pane = new JTabbedPane() {
			private static final long serialVersionUID = 1L;

			@Override
			protected void paintComponent(Graphics g) {
				for (int i = 0; i < getTabCount(); i++) {
					if (getTabComponentAt(i) == null)
						continue;
					g.fillRect(10 + i * getTabComponentAt(i).getWidth(), 0, getTabComponentAt(i).getWidth(), 50);
				}
			}
		};
		pane.setBounds(getWidth() / 2 - LauncherWindow.LOGIN_SNIP_WIDTH / 2 - 2,
				getHeight() / 2 - LauncherWindow.LOGIN_SNIP_HEIGHT / 2 - LauncherWindow.LOGIN_PANE_OFFSET_Y, 410, 483);

		pane.setPreferredSize(new Dimension(380, 410));
		pane.setMinimumSize(new Dimension(380, 410));

		loginButton.setLocation(getWidth() / 2 - LauncherWindow.LOGIN_SNIP_WIDTH / 2 + LOGIN_BUTTON_LOGIN_OFFSET_X,
				getHeight() / 2 - LauncherWindow.LOGIN_SNIP_HEIGHT / 2);
		registerButton.setLocation(
				getWidth() / 2 - LauncherWindow.LOGIN_SNIP_WIDTH / 2 + LOGIN_BUTTON_REGISTER_OFFSET_X,
				getHeight() / 2 - LauncherWindow.LOGIN_SNIP_HEIGHT / 2);

		result.add(loginButton);
		result.add(registerButton);
		result.add(pane);

		loginState = new JPanel();
		loginState.setBackground(new Color(LauncherWindow.color_bg));
		loginState.setLayout(null);
		pane.addTab("Login", loginState);

		loginLabel = new AquaCoreLabel("Логин");
		loginLabel.setForeground(new Color(LauncherWindow.color_accent));
		loginLabel.setBounds(STATE_OFF_X, 20, 46, 35);
		loginState.add(loginLabel);

		loginField = new AquaCoreTextField(loginLabel);
		loginField.setBackground(new Color(LauncherWindow.color_bg));
		loginField.setBounds(STATE_OFF_X, 50, 200, 30);
		loginField.setText(LauncherProperties.instance.getProperty("username"));
		loginState.add(loginField);

		passwordLabel = new AquaCoreLabel("Пароль");
		passwordLabel.setForeground(new Color(LauncherWindow.color_accent));
		passwordLabel.setBounds(STATE_OFF_X, 90, 167, 30);
		loginState.add(passwordLabel);

		passwordField = new AquaCorePassField(passwordLabel);
		passwordField.setBackground(new Color(LauncherWindow.color_bg));
		passwordField.setBounds(STATE_OFF_X, 120, 200, 30);
		passwordField.setText(new String(Base64.decodeBase64(LauncherProperties.instance.getProperty("password"))));
		loginState.add(passwordField);
		passwordField.setColumns(10);

		l_loginButton = new AquaCoreFlatButton("Войти (?/?)", 1);
		l_loginButton.setBounds(STATE_OFF_X, 260, 200, 30);
		loginState.add(l_loginButton);

		l_selectClient = new AquaCoreLabel("Сервер");
		l_selectClient.setForeground(new Color(LauncherWindow.color_accent));
		l_selectClient.setBounds(STATE_OFF_X, 160, 167, 30);
		loginState.add(l_selectClient);

		l_scCbox = new JComboBox<>();
		l_scCbox.setBounds(STATE_OFF_X, 190, 200, 30);
		l_scCbox.addItem(new ClientBox());
		new Thread(new Runnable() {

			@Override
			public void run() {
				final List<ClientBox> cbl = AuthHelper.receiveClientList();
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						l_scCbox.removeAllItems();
						if (cbl == null) {
							ClientBox nocb = new ClientBox();
							nocb.id = "";
							nocb.name = "Ошибка";
							nocb.pingHost = "188.18.54.44";
							nocb.pingPort = 25800;
							l_scCbox.addItem(nocb);
						} else {
							for (ClientBox cb : cbl) {
								l_scCbox.addItem(cb);
								if (cb.id.equals(LauncherProperties.instance.getProperty("client"))) {
									l_scCbox.setSelectedItem(cb);
									l_loginButton.setText("Войти (" + cb.status + ")");
								}
							}
							l_scCbox.addItemListener(new ItemListener() {

								@Override
								public void itemStateChanged(ItemEvent e) {
									if (e.getStateChange() == ItemEvent.SELECTED) {
										if (e.getItem() != null) {
											LauncherProperties.instance.setProperty("client",
													((ClientBox) e.getItem()).id);
											LauncherProperties.instance.store();
											Main.main(new String[] {});
										}
									}
								}

							});

						}
					}
				});

			}
		}).start();

		l_scCbox.setRenderer(new AquaCoreLCR());

		repaint();

		loginState.add(l_scCbox);

		l_error = new AquaCoreLabel("Неверный пароль");
		l_error.setVisible(false);
		l_error.setForeground(Color.RED);
		l_error.setBounds(STATE_OFF_X, 223, 200, 35);
		loginState.add(l_error);

		l_settingsButton = new AquaCoreFlatButton("Настройки");
		l_settingsButton.setBounds(STATE_OFF_X, 310, 200, 30);
		l_settingsButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setStateSettings();
			}
		});
		loginState.add(l_settingsButton);

		monitorUpdateThread.start();

		changeSkinButton = new AquaCoreFlatButton("...");
		changeSkinButton.setText("Сменить скин");
		changeSkinButton.setBounds(100, 360, 199, 30);
		loginState.add(changeSkinButton);

		regState = new JPanel();
		regState.setBackground(new Color(LauncherWindow.color_bg));
		regState.setLayout(null);
		pane.addTab("Register", regState);

		regState.add(loginLabel);
		regState.add(loginField);
		regState.add(passwordLabel);
		regState.add(passwordField);

		r_registerButton = new AquaCoreFlatButton("Зарег-ться", 1);
		r_registerButton.setBounds(STATE_OFF_X, 310, 200, 30);
		regState.add(r_registerButton);

		r_confirmPassword = new AquaCoreLabel("Подтвердите пароль");
		r_confirmPassword.setForeground(new Color(LauncherWindow.color_accent));
		r_confirmPassword.setBounds(STATE_OFF_X, 160, 212, 35);
		regState.add(r_confirmPassword);

		r_confirmPasswordField = new AquaCorePassField(r_confirmPassword);
		r_confirmPasswordField.setBackground(new Color(LauncherWindow.color_bg));
		r_confirmPasswordField.setColumns(10);
		r_confirmPasswordField.setBounds(STATE_OFF_X, 190, 200, 30);
		regState.add(r_confirmPasswordField);

		r_error = new AquaCoreLabel("Пароли не совпадают");
		r_error.setVisible(false);
		r_error.setForeground(Color.RED);
		r_error.setBounds(STATE_OFF_X, 243, 181, 35);
		regState.add(r_error);

		regState.add(changeSkinButton);

		changeSkinButton.addActionListener(changeSkinListener);
		loginButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				onLoginSwitch();
			}
		});

		registerButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				onRegisterSwitch();
			}
		});

		onLoginSwitch();
		return result;
	}

	private void onLoginSwitch() {
		loginButton.changeState(0);
		registerButton.changeState(1);
		pane.setSelectedIndex(0);

		loginState.add(loginLabel);
		loginState.add(loginField);
		loginState.add(passwordLabel);
		loginState.add(passwordField);
		loginState.add(changeSkinButton);
	}

	private void onRegisterSwitch() {
		loginButton.changeState(1);
		registerButton.changeState(0);
		pane.setSelectedIndex(1);

		regState.add(loginLabel);
		regState.add(loginField);
		regState.add(passwordLabel);
		regState.add(passwordField);
		regState.add(changeSkinButton);

	}

	/**
	 * Creating the Loading panel
	 * 
	 * @return JPanel - the loading panel.
	 **/
	private BgPane createLoadingPane() {
		BgPane result = new BgPane() {

			private static final long serialVersionUID = 1L;

			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);

				g.setColor(new Color(LauncherWindow.color_bg));
				g.fillRect(this.getWidth() / 2 - LOADING_WIDTH / 2, this.getHeight() / 2 - LOADING_HEIGHT / 2,
						LOADING_WIDTH, LOADING_HEIGHT);

			}

		};

		loadingBar = new JProgressBar(0, 100);
		loadingBar.setBorderPainted(false);
		loadingBar.setForeground(new Color(LauncherWindow.color_daccent));
		loadingBar.setBackground(new Color(LauncherWindow.color_dbg));
		loadingBar.setMinimum(0);
		loadingBar.setMaximum(100);
		loadingBar.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		loadingBar.setBounds(getWidth() / 2 - LOADING_WIDTH / 2 + 25, getHeight() / 2 - LOADING_HEIGHT / 2 + 30, 350,
				30);
		result.add(loadingBar);

		loadingLabel = new AquaCoreLabel("Загрузка... " + loadingBar.getValue() + "%");
		loadingLabel.setBounds(getWidth() / 2 - LOADING_WIDTH / 2 + 25, getHeight() / 2 - LOADING_HEIGHT / 2,
				LOADING_WIDTH - 50, 30);
		loadingLabel.setForeground(new Color(LauncherWindow.color_accent));
		result.add(loadingLabel);

		return result;
	}

	/**
	 * Creating the skin chooser panel
	 * 
	 * @return JPanel - the loading panel.
	 **/
	private BgPane createSkinsPane() {
		BgPane result = new BgPane();

		skinView = new Canvas();
		skinView.setBackground(new Color(32, 32, 32));
		result.add(skinView);

		backButton = new AquaCoreFlatButton("Назад");
		backButton.setSize(200, 35);
		backButton.addActionListener(backListener);
		result.add(backButton);

		return result;
	}

	private void updateTitleSize() {
		titlebar.setSize(getWidth(), titlebar.getHeight());
		closeButton.setLocation(getWidth() - closeButton.getWidth(), (int) closeButton.getLocation().getY());
		minimizeButton.setLocation(getWidth() - closeButton.getWidth() - minimizeButton.getWidth(),
				(int) closeButton.getLocation().getY());
		background.setSize(getWidth() - closeButton.getWidth() - minimizeButton.getWidth(), background.getWidth());
	}

	private void updateContentSize() {
		gamePanel.setSize(getWidth() - 4, getHeight() - titlebar.getHeight() - 2);
		gamePanel.setLocation(2, titlebar.getHeight());
		switch (state) {
		case LauncherWindow.STATE_LOGIN:
			loginPanel.setSize(getWidth(), getHeight() - titlebar.getHeight());

			loginButton.setLocation(getWidth() / 2 - LauncherWindow.LOGIN_SNIP_WIDTH / 2 + LOGIN_BUTTON_LOGIN_OFFSET_X,
					getHeight() / 2 - LauncherWindow.LOGIN_SNIP_HEIGHT / 2);
			registerButton.setLocation(
					getWidth() / 2 - LauncherWindow.LOGIN_SNIP_WIDTH / 2 + LOGIN_BUTTON_REGISTER_OFFSET_X,
					getHeight() / 2 - LauncherWindow.LOGIN_SNIP_HEIGHT / 2);
			pane.setLocation(getWidth() / 2 - LauncherWindow.LOGIN_SNIP_WIDTH / 2 - 2,
					getHeight() / 2 - LauncherWindow.LOGIN_SNIP_HEIGHT / 2 - LauncherWindow.LOGIN_PANE_OFFSET_Y);

			break;
		case LauncherWindow.STATE_LOADING:
			loadingLabel.setLocation(getWidth() / 2 - LOADING_WIDTH / 2 + 25, getHeight() / 2 - LOADING_HEIGHT / 2);
			loadingBar.setLocation(getWidth() / 2 - LOADING_WIDTH / 2 + 25, getHeight() / 2 - LOADING_HEIGHT / 2 + 30);
			loadingPanel.setBounds(0, 30, getWidth(), getHeight() - 30);
			break;
		case LauncherWindow.STATE_SKINS:
			skinsPanel.setSize(getWidth(), getHeight() - titlebar.getHeight());
			skinView.setBounds(SKINS_CANVAS_INSETS, SKINS_CANVAS_INSETS, getWidth() - SKINS_CANVAS_INSETS * 2,
					getHeight() - SKINS_CANVAS_INSETS * 3);
			backButton.setLocation(SKINS_CANVAS_INSETS + SKINS_BUTTON_OFFX, getHeight() - SKINS_CANVAS_INSETS - 50);
			break;
		case LauncherWindow.STATE_SETTINGS:
			backButton.setLocation(getWidth() / 2 - SETTINGS_WIDTH / 2 + 50,
					getHeight() / 2 - SETTINGS_HEIGHT / 2 + 400);
			backButton.setSize(100, 35);
			memoryLabel.setBounds(getWidth() / 2 - SETTINGS_WIDTH / 2 + 50, getHeight() / 2 - SETTINGS_HEIGHT / 2 + 30,
					300, 50);
			memoryStateLabel.setBounds(getWidth() / 2 - SETTINGS_WIDTH / 2 + 50,
					getHeight() / 2 - SETTINGS_HEIGHT / 2 + 100, 300, 50);

			setter.setBounds(getWidth() / 2 - SETTINGS_WIDTH / 2 + 50, getHeight() / 2 - SETTINGS_HEIGHT / 2 + 80, 300,
					20);
			settingsPanel.setBounds(0, 30, getWidth(), getHeight() - 30);
			settingsPanel.setBorder(new EmptyBorder(0, 0, 0, 0));

			st_saveButton.setLocation(getWidth() / 2 - SETTINGS_WIDTH / 2 + 160,
					getHeight() / 2 - SETTINGS_HEIGHT / 2 + 400);
			st_saveButton.setSize(190, 35);
			break;
		}
	}
}
